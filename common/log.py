"""! @file


@package common
"""

import os
import time
import logging
import logging.config


DEPLOYED = False

class GMTFormatter(logging.Formatter):
    converter = time.gmtime

def setup_logging(filename: str = 'data_etl_pipelines'):
    lowest_level = 'DEBUG'
    if DEPLOYED:
        lowest_level = 'INFO'
    logging.config.dictConfig({
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'simple': {
                'format': '%(asctime)s %(levelname)-8s %(message)s'
            },
            'detail': {
                'format': '%(asctime)s - [%(levelname)s] - File: %(filename)s - Func: %(funcName)s() - Line: %(lineno)d %(message)s',
                '()': GMTFormatter
            } # ,
            # 'json': {
            #     'format': '%(asctime)s %(levelname)s %(filename)s %(funcName)s %(lineno)d %(message)s',
            #     'class': 'pythonjsonlogger.jsonlogger.JsonFormatter'
            # }
        },
        'handlers': {
            'stream': {
                'class': 'logging.StreamHandler',
                'level': lowest_level,
                'formatter': 'simple'
            },
            'file_handler': {
                'class': 'logging.handlers.TimedRotatingFileHandler',
                'level': 'ERROR',
                'formatter': 'detail',
                'filename': '../logs/{}.log'.format(filename),
                'interval': 1,
                'when': 'midnight',
                'backupCount': 10,
                'utc': True,
                'encoding': 'utf8'
            },
            # 'bugsnag': {
            #     'class': 'bugsnag.handlers.BugsnagHandler',
            #     'level': 'ERROR'
            # },
            # 'json_handler': {
            #     'class': 'logging.handlers.RotatingFileHandler',
            #     'level': 'ERROR',
            #     'formatter': 'json',
            #     'filename': '../logs/{}.json'.format(filename),
            #     'maxBytes': 2097152,
            #     'backupCount': 10
            # },
            'debug_handler': {
                'class': 'logging.handlers.RotatingFileHandler',
                'level': lowest_level,
                'formatter': 'detail',
                'filename': '../logs/{}-info.log'.format(filename),
                'maxBytes': 2097152,
                'backupCount': 10
            }
        },
        'root': {
            'handlers': ['stream', 'file_handler', 'debug_handler'], # 'json_handler', 'bugsnag', 
            'level': lowest_level
        }
    })

    return logging.getLogger()

def SetLogFile(filename):
    """! Deprecated. Use setup_logging."""
    return setup_logging(filename)