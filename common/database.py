"""! @file

# Database

Helper functions for loading and pulling from Postgres DBs.

@package common """

##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))
##\endcond

import psycopg2	 # for PostgreSQL connections
import pandas as pd
from datetime import datetime

HOST='0.0.0.0'
USER='postgres'
DB='postgres'
PORT=5432
OPTIONS=None

def GetDBConnector(SCHEMA=None):
	"""! Connect to Docker postgres db"""

	mydb = psycopg2.connect(
		host=os.environ.get('ALGO_DB'),
		user=USER,
		password=os.environ.get('ALGO_DB_PW'),
		database=DB,
		port=PORT,
		options=OPTIONS
	)

	return mydb

def QueryDatabase(query, SCHEMA=None):
	"""! Query the database. """
	mydb = GetDBConnector(SCHEMA=SCHEMA)
	mycursor = mydb.cursor()

	mycursor.execute(query)
	res = mycursor.fetchall()

	mycursor.close()
	mydb.close()

	return res

def ExecuteOnDatabase(query):
	try:
		mydb = GetDBConnector()
		mycursor = mydb.cursor()

		mycursor.execute(query)
		mydb.commit()

	except (Exception, psycopg2.Error) as error:
		logger.error("Error in update operation", exc_info=True)
		logger.error(query)

	finally:
		# closing database connection.
		if (mydb):
			mycursor.close()
			mydb.close()

def GetTable(table_name, WHERE=None, SUFFIX=''):
	"""! Retrieve the contents of a table with optional WHERE clause criteria
	@param table_name: (str) the table name to retrieve data from
	@param WHERE: (str) optional PostgreSQL WHERE clause criteria (ex: 'created_at < NOW()')
	@param SUFFIX: (str) optional clause to attach at the end of the query (ex: "ORDER BY time", "GROUP BY user_id")

	@return: list of dictionaries for each entry in the table.
	"""
	query = "SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_NAME = '{}' AND table_schema = 'public' ORDER BY ordinal_position".format(table_name)
	schema = QueryDatabase(query)

	# we can't just request * because the columns might not arrive in the same order. So we explicitly request the columns from the information_schema
	col_str = ""
	for s in schema:
		col_str += '"{}",'.format(s[0])

	if col_str == "":
		raise ValueError('table {} cannot be found in database'.format(table_name))

	query = """SELECT {} FROM "public"."{}" """.format(col_str[:len(col_str)-1], table_name)
	if not WHERE is None:
		query += " WHERE {}".format(WHERE)
	query += " " + SUFFIX

	data = QueryDatabase(query)

	table = []
	for da in data:
		entry = {}
		for s, d in zip(schema, da):
			entry[s[0]] = d
		table.append(entry)
	return table

def TableFromDataFrame(df, table_name, INDEX='id', DEFAULT_COL_TYPE='text'):
	"""!
	@param df: a data frame to use as the schematic for a table
	@table_name: the name for the table
	@INDEX: the column name to use as the unique index (default='id')
	@DEFAULT_COL_TYPE: default type for a column if the DF dtype is 'object' (default='text')
	"""
	import pandas as pd

	if table_name is None or table_name == "" or len(df.columns) < 1:
		return

	df = PrepareDataFrame(df, table_name)

	from common.database import GetDBConnector
	mydb = GetDBConnector()
	mycursor = mydb.cursor()

	mycursor.execute('CREATE TABLE IF NOT EXISTS "public"."{}" ("{}_index" serial,"created_at" timestamp DEFAULT NOW(),"updated_at" timestamp DEFAULT NOW(), PRIMARY KEY ("{}_index"))'.format(table_name, table_name, table_name))

	for c in df.columns:
		name = c
		cmd = 'ALTER TABLE "public"."{}" ADD COLUMN IF NOT EXISTS "{}" '.format(table_name, name)
		
		# to find the type of the column: filter out not null entries, convert to list, and get first item.
		x = df.loc[df[c].notnull()][c].tolist()

		if len(x) < 1:
			cmd += DEFAULT_COL_TYPE
		else:
			cmd += TypeFromElement(x[0])

		mycursor.execute(cmd)
		
		if c == INDEX:
			query = 'CREATE UNIQUE INDEX IF NOT EXISTS "{}_id_key"'.format(table_name) + \
				' ON "public"."{}"'.format(table_name) + \
				' USING BTREE ("{}")'.format(c)
			mycursor.execute(query)

	mydb.commit()

def DataFrameToDatabase(df, table_name, APPEND=True, INDEX='id'):
	"""!
	@param df: a data frame to load into a table
	@table_name: the name of the table to load into. It will be created if it does not exist.
	@param APPEND: if True (default) new records will be appended to the table, otherwise the table will be dropped and recreated.
	@param INDEX: name of the primary key for the dataframe
	"""
	import json
	from datetime import datetime

	TableFromDataFrame(df, table_name, INDEX=INDEX)
		
	if INDEX is None:
		INDEX = '{}_index'.format(table_name)

	command = ''
	col = df.columns
	count = 0
	for idx, row in df.iterrows():

		try:
			if INDEX in row:
				ix = row[INDEX]

				if pd.isnull(ix):
					continue # can't have null index, just skip

				ix = _formatElement(ix)

				cmd = 'INSERT INTO "public"."{}" ("{}", "created_at", "updated_at") VALUES ({}, NOW(), NOW()) ON CONFLICT ("{}") DO UPDATE SET updated_at = NOW();'.format(table_name, INDEX, ix, INDEX)
			
				command += cmd

				val_str = ""
				
				for c in col:
					if c == INDEX:
						continue

					val_str += '"{}"='.format(c)

					val_str += _formatElement(row[c]) + ","
					
				cmd = 'UPDATE "public"."{}" SET {} WHERE "{}"={};'.format(table_name, val_str[:len(val_str)-1], INDEX, ix)
				
				command += cmd
			else: # no index, just insert don't update
				col_str = ""
				val_str = ""
				for c in col:
					col_str += '"{}",'.format(c)
					val_str += _formatElement(row[c]) + ","

				cmd = """INSERT INTO "public"."{}" ({}) VALUES ({});""".format(table_name, col_str[:len(col_str)-1], val_str[:len(val_str)-1])
				command += cmd
			
			if len(command) > 65000:
				ExecuteOnDatabase(command)
				command = ''

		except Exception as e:
			raise ValueError(row)

	if command != '':
		ExecuteOnDatabase(command)

def PrepareDataFrame(df, table_name, AS_IS=False):
	"""! Perform cleanup and preparation steps prior to loading into the destination database. Catch reserved column names and swap.

	@param df: dataframe to cleanup
	@param table_name: name of the table the dataframe is going to be loaded into
	@param IS_REDSHIFT: if True then check for reserved words in column names and prepend table_name if found
	@param AS_IS: if False will prepend table_name if updated_at, created_at are in the dataframe, so a new updated_at, created_at can be added on the load.
	"""
	col = df.columns.to_list()

	if not AS_IS:
		# catch reserved column names: created_at, updated_at, ts
		if 'updated_at' in col:
			try:
				df = df.rename(columns = {'updated_at': '{}_updated_at'.format(table_name)})
			except:
				pass
		if 'created_at' in col:
			try:
				df = df.rename(columns = {'created_at': '{}_created_at'.format(table_name)})
			except:
				pass

	else:
		df = FixDatetimeDType(df)

	return df

#### Functions for Interfacing with Postgres ####
def get_column_data_types(data_frame, index=False):
	"""! given a data frame, infer the column types.
	Originally from pandas_redshift @see https://github.com/agawronski/pandas_redshift/blob/68c899676600c3b072d069f46e530c8579cce23c/pandas_redshift/core.py#L164
	"""
	column_data_types = [pd_dtype_to_postgres_dtype(dtype)
						 for dtype in data_frame.dtypes.values]
	if index:
		column_data_types.insert(
			0, pd_dtype_to_postgres_dtype(data_frame.index.dtype))

	return column_data_types

def TypeFromElement(x):
	"""! 
	@param ISINDEX: set true if this is a request for the index type, rather than an actual row element. If set true and a string contains a date string it will return 'timestamp', otherwise strings return 'text'.
	"""
	import pandas as pd

	ret = 'text'

	try:
		t = type(x)
		if t is str:
			# test if it's actually a date string:
			# if is_date(x):
			# 	ret = 'timestamp'
			# else:
			ret = 'text'
		elif t is int:
			ret = 'int8'
		elif t is list or t is dict:
			ret = 'jsonb'
		elif t is bool:
			ret = 'bool'
		elif t is float:
			ret = 'float8'
		elif pd.core.dtypes.common.is_datetime_or_timedelta_dtype(x) or t is pd.Timestamp or t is datetime: #test if it is any form of date
			ret = 'timestamp'
		elif t == pd._libs.tslibs.period.Period: # in postgres we're going to treat Periods as timestamps
			ret = 'timestamp'

		return ret
	except:
		logger.error('TypeFromElement: {} is {}'.format(x, t), exc_info=True)

def _formatElement(x):
	import json
	import pandas as pd
	from dateutil.parser import parse

	t = TypeFromElement(x)
	if t != 'jsonb' and pd.isnull(x): # 'text' is the catch all, we can't test for null if x is a dictionary or list
		ret = "Null"
	else:
		if t == 'text':
			ret =  "'{}'".format(x.replace("'","''"))
		elif t == 'jsonb':
			j = json.dumps(x)
			j = j.replace(r"\\", "")
			j = j.replace(r"'", r"''")
			ret = "'{}'".format(j)
		elif t == 'timestamp':
			if type(x) is str:
				y = parse(x)
			elif type(x) == pd._libs.tslibs.period.Period:
				y = x.to_timestamp()
			else:
				y = x
			ret = "'{}'".format(y.strftime("%Y-%m-%d %H:%M:%S"))
		else:
			ret = '{}'.format(x)

	return ret

def pd_dtype_to_postgres_dtype(dtype):
	"""! from a dtype find the column type.

	TODO: will this catch Period as a TIMESTAMP?
	"""
	import pandas as pd
	import json

	name = dtype.name

	if name.startswith('int64'):
		return 'int8'
	elif name == 'object':
		return 'text' # could be jsonb too... but that's hard to determine accurately. text will hold everything
	elif name.startswith('int'):
		return 'int8'
	elif name.startswith('float'):
		return 'float8'
	elif name.startswith('datetime') or type(dtype) is pd.Timestamp or pd.core.dtypes.common.is_datetime_or_timedelta_dtype(dtype) or dtype == pd._libs.tslibs.period.Period:
		return 'TIMESTAMP'
	elif name == 'bool':
		return 'BOOLEAN'
	else:
		return 'text'

def DeDupeTable(table_name, keys=None):
	"""! Pull any duplicates from table_name and delete all but the newest entry
	"""
	if table_name is None or table_name == '' or keys is None:
		return False

	key_str = ""
	for k in keys:
		key_str += '"{}",'.format(k)
	key_str = key_str[:len(key_str)-1]

	try:
		# get duplicate entries, put in a temp table:
		ExecuteOnDatabase("""DROP TABLE IF EXISTS "{}_duplicates";""".format(table_name))

		cmd = """CREATE TABLE "{}_duplicates" AS
				SELECT * FROM (
					SELECT
						{},
						"updated_at",
						ROW_NUMBER() OVER (PARTITION BY {}
							ORDER BY "updated_at" DESC) AS dupcnt
					FROM
						(SELECT * FROM "{}" ORDER BY "updated_at" DESC) t1
				) t2
				WHERE
					dupcnt > 1;""".format(table_name, key_str, key_str, table_name)

		# log.info(cmd)
		ExecuteOnDatabase(cmd)

		cmd = """DELETE FROM "{}"
				USING  "{}_duplicates" t2
				WHERE t2.updated_at = "{}".updated_at AND """.format(table_name, table_name, table_name)

		for k in keys:
			cmd += 't2."{}" = "{}"."{}" AND '.format(k, table_name, k)

		# log.info(cmd[:len(cmd)-5])
		ExecuteOnDatabase(cmd[:len(cmd)-5])

		# clean up:
		ExecuteOnDatabase("""DROP TABLE IF EXISTS "{}_duplicates";""".format(table_name))
	except:
		logger.error('common.loader:DeDupeRedshift table_name={}, keys={}'.format(table_name, keys), exc_info = True)