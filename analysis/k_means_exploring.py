


##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))
##\endcond

import pandas as pd # for dataset handling
import json
from time import time
import numpy as np
import matplotlib.pyplot as plt

from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
from sklearn.preprocessing import StandardScaler

from common.database import GetTable


CLUSTER_COUNT = 6


def bench_k_means(estimator, name, data):
    t0 = time()
    estimator.fit(data)
    print('%-9s\t%.2fs\t%i\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f'
          % (name, (time() - t0), estimator.inertia_,
             metrics.homogeneity_score(labels, estimator.labels_),
             metrics.completeness_score(labels, estimator.labels_),
             metrics.v_measure_score(labels, estimator.labels_),
             metrics.adjusted_rand_score(labels, estimator.labels_),
             metrics.adjusted_mutual_info_score(labels,  estimator.labels_),
             metrics.silhouette_score(data, estimator.labels_,
                                      metric='euclidean',
                                      sample_size=sample_size)))

if __name__ == "__main__":
    bars = GetTable('finnhub_volume_bars_60', SUFFIX="LIMIT 100000")
    bars = pd.DataFrame(bars)

    feat_columns = [col for col in bars.columns \
                if 'flag' in col or 'entropy' in col or 'bid' in col or 'spread' in col or 'hour' in col \
               or 'delta' in col or 'beta' in col]

    # build training set outcomes
    data = np.array(bars[feat_columns])
    # labels = np.ravel(feat_columns)
    data = pd.DataFrame(data).fillna(0)

    # now fit and transform the training and test data
    sc = StandardScaler()  
    X = sc.fit_transform(data)  

    # perform the clustering.
    km = KMeans(init='k-means++', n_clusters=CLUSTER_COUNT, n_init=10)	# n_init: how many times to perform the fit, returning best
    #bench_k_means(km, name="k-means++", data=X)
    km.fit(data)

    # print out the centers
    centers = km.cluster_centers_
    centers = sc.inverse_transform(centers)
    for cent in centers:
      print(cent.tolist()) #.astype(float))

    # save as a file
    cent_dict = {}
    cent_dict['centroids'] = centers.tolist()
    with open('./tmp/centroids.json', 'w') as outfile:
        json.dump(cent_dict, outfile)

    # how many accounts belong to each cluster?
    Z = km.predict(X)
    count = [0] * CLUSTER_COUNT	# one element for each cluster
    for val in Z:
    	count[val] += 1
    print("cluster membership:", count)

    # bench_k_means(KMeans(init='random', n_clusters=8, n_init=10),
    #               name="random", data=data)

    # # in this case the seeding of the centers is deterministic, hence we run the
    # # kmeans algorithm only once with n_init=1
    # pca = PCA(n_components=8).fit(data)
    # bench_k_means(KMeans(init=pca.components_, n_clusters=8, n_init=1),
    #               name="PCA-based",
    #               data=data)
    print(82 * '_')

    #Second half here is to produce a simple graph on the first 2 PCA components to show something about the data (mostly find outliers)

    # perform a PCA analysis
    reduced_data = PCA(n_components=2).fit_transform(X)  

    # explained_variance = pca.explained_variance_ratio_
    # print("explained variance", explained_variance)

    kmeans = KMeans(init='k-means++', n_clusters=CLUSTER_COUNT, n_init=10)
    kmeans.fit(reduced_data)

    # Step size of the mesh. Decrease to increase the quality of the VQ.
    h = .02     # point in the mesh [x_min, x_max]x[y_min, y_max].

    # Plot the decision boundary. For that, we will assign a color to each
    x_min, x_max = reduced_data[:, 0].min() - 1, reduced_data[:, 0].max() + 1
    y_min, y_max = reduced_data[:, 1].min() - 1, reduced_data[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    # Obtain labels for each point in mesh. Use last trained model.
    Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    plt.figure(1)
    plt.clf()
    plt.imshow(Z, interpolation='nearest',
               extent=(xx.min(), xx.max(), yy.min(), yy.max()),
               cmap=plt.cm.Paired,
               aspect='auto', origin='lower')

    plt.plot(reduced_data[:, 0], reduced_data[:, 1], 'k.', markersize=2)
    # Plot the centroids as a white X
    centroids = kmeans.cluster_centers_

    plt.scatter(centroids[:, 0], centroids[:, 1],
                marker='x', s=169, linewidths=3,
                color='w', zorder=10)
    plt.title('K-means clustering on the digits dataset (PCA-reduced data)\n'
              'Centroids are marked with white cross')
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    plt.xticks(())
    plt.yticks(())
    plt.show()