"""! @file

Implements a simple cumulative sum filter for a series of raw observations.

Everytime the series has moved further than a given threshold (h) the series is sampled,
and the marker is reset. Cumulatively sums positive and negative movement seperately
to sample thresholds in both directions.

h - is calculated by finding the variance of the series over the entire range and dividing by 10.
Creates samples every 1/10th of the range. Could also calculate as a percentage of the price,
Say every 5% change in price triggers the threshold. Something that could be meaningful as a trade
event trigger.

See Advances in Financial Machine Learning, chapter 2

@package features"""

##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))
##\endcond

import pandas as pd
import numpy as np

SYMBOL = 'AAPL'
TYPE = 'volume'

if len(sys.argv) > 1:
	SYMBOL = sys.argv[1].upper()
if len(sys.argv) > 2:
	TYPE = 'dollar'

def getTEvents(gRaw: pd.DataFrame, h: float) -> pd.DataFrame:
	"""! 
	@param gRaw: a time series of raw observations we wish to filter
	@param h: change threshold to trigger the filter conditions, in gRaw units
	"""
	tEvents, flags, sPos, sNeg = [], [], 0, 0

	diff = gRaw.diff().fillna(0)

	for i in diff.index[1:]:
		sPos, sNeg = max(0, sPos + diff.loc[i]), min(0, sNeg + diff.loc[i])

		if sNeg < -h:
			sNeg = 0
			tEvents.append(i)
			flags.append(0)
		elif sPos > h:
			sPos = 0
			tEvents.append(i)
			flags.append(1)

	return tEvents, flags # pd.DatetimeIndex(tEvents)

def CSWTest(gRaw: pd.DataFrame, b = 4.6, n = 1) -> pd.DataFrame:
	"""!
	See Advances in Financial Machine Learning, p. 251.

	@param gRaw: sequence of observations
	@param b: a coefficient determined to obtain 95% confidence. b=4.6 according to the literature.
	@param n: the window size. 

	"""
	df = pd.DataFrame()
	df['y'] = gRaw

	# compute deltas:
	df['delta'] = gRaw.diff()
	y_sum = sum(pow(df['delta'].dropna(), 2.0)) # used for time weighted standard deviation calculation
	
	df['sigma'], df['S'], df['critical'] = np.zeros(len(gRaw)), np.zeros(len(gRaw)), np.zeros(len(gRaw))

	# compute sigma at each point:
	t = 0
	for idx, row in df.iterrows():
		t += 1
		if t < 2:
			continue

		sigma = pow( pow(t-1, -1) * y_sum, 0.5 )

		row['sigma'] = sigma

		# calculate the test level S_n,t
		if t > n:
			S = (row['y'] - df.iloc[n]['y']) * pow((row['sigma'] * pow(t-n, 0.5)), -1)

			row['S'] = S

			# calculate the time dependent critical value:
			row['critical'] = pow( b + np.log2(t-n), 0.5 ) 

	return df

def DoCSWTests(bars):

	max_S, max_n = (0,0)
	for i in np.linspace(0, len(bars), 100):
		df = CSWTest(bars['c_log'], n=int(i))

		if max(df['S']) > max_S:
			max_S = max(df['S'])
			max_n = int(i)

	# run again and plot
	print("running again with n={} to get max S={}".format(max_n, max_S))
	df = CSWTest(bars['c_log'], n=max_n)

	import matplotlib.pyplot as plt
	# df['delta'].plot()
	# df['sigma'].plot()
	# plt.show()

	bars['c_log'].plot()
	plt.twinx()
	# plt.plot(df['sigma'], 'r')
	plt.plot(df['S'],'g')
	plt.plot(df['critical'],'y')
	plt.plot(-df['critical'], 'y')
	plt.show()

if __name__ == "__main__":
	import matplotlib.pyplot as plt

	from data_etl.dollar_bars import GetBars
	bars = GetBars(SYMBOL, sample_factor=1, bar_type=TYPE, resolution="D")
	print(bars.head())

	# from common.database import GetTable

	# candles = GetTable("finnhub_stock_candles_D", WHERE="symbol = '{}'".format(SYMBOL), SUFFIX="ORDER BY t")

	# from data_etl.dollar_bars import TimeToVolume

	# bars = pd.DataFrame(TimeToVolume(candles, SAMPLE_FACTOR=1, TYPE=TYPE)) # ToVolume returns 't' as a serial index, we could approximate dates if we wanted using candles start/end dates
	
	# candles = pd.DataFrame(candles)
	# scale = (max(candles['t']) - min(candles['t'])) / max(bars['t'])

	# bars['ts'] = pd.to_datetime(bars['t'] * scale + min(candles['t']), unit='s')

	# this maybe already done, but would be redundant at worst.
	# bars.set_index(pd.to_datetime(bars["ts"], unit='s'), inplace=True)

	# use log prices to account for wide swings over years of data. Then plot on raw price data.
	bars['h_log'] = np.log2(bars['h'])
	bars['l_log'] = np.log2(bars['l'])
	bars['c_log'] = np.log2(bars['c'])

	if 0:
		DoCSWTests(bars)
	
	if 1:
		bar_range = max(bars['h_log']) - min(bars['l_log'])

		h = bar_range / 10

		idx, flags = getTEvents(bars['c_log'], h)

		# since volume is constant, delta-time will vary. Use up/down movement in delta-time to determine places other players are entering/exiting the market!
		bar_range = bars['ts'].diff().fillna(0)
		bar_range += min(bar_range.iloc[1:]) # add DC offset so all x >= 0

		h = (max(bar_range) - min(bar_range.iloc[1:])) * 0.5

		print(bar_range)

		idx2 = getTEvents(bar_range.fillna(0), h)

		plt.plot(bars['c'])
		plt.plot(bars.loc[idx]['c'], 'go')
		plt.plot(bars.loc[idx2]['c'], 'yo')
		plt.show()