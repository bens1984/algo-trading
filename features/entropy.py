"""! @file

Entropy Estimators

@package features"""

##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))

import pandas as pd
import numpy as np
# import matplotlib.pyplot as plt
from datetime import datetime, timedelta

SYMBOL = 'AAPL'
TYPE = 'volume'
D = None

if len(sys.argv) > 1:
	SYMBOL = sys.argv[1].upper()
if len(sys.argv) > 2 and sys.argv[2].find('d') > -1:
	TYPE = 'dollar'
if len(sys.argv) > 3:
	D = float(sys.argv[3])
##\endcond

def konto(msg, window=None, memory=None):
	"""!
	Kontoyiannis' LZ entropy estimate, 2013 version (centered window)
	Inverse of the avg length of the shortest non-redundant substring.
	If non-redundant substrings are short, the text is highly entropic.
	If the end of msg is more relevant, try konto(msg[::-1])
	@param window: == None for expanding window, in which case len(msg)%2==0
	@param memory: the output of a previous konto call. Uses the previous dictionary and extends it.
	"""
	if memory is None:
		out = {'num':0, 'sum':0, 'subS':[]}
	else:
		out = memory
	if not isinstance(msg, str):
		msg = ''.join(map(str,msg))
	if window is None:
		points = range(1, round(len(msg)/2)+1)
	else:
		window = min(window, len(msg)/2)
		points = range(window, len(msg)-window+1)
	for i in points:
		if window is None:
			l, msg_ = matchLength(msg, i, i) # to avoid Doeblin condition
			out['sum'] += np.log2(i+1)/l
		else:
			l, msg_ = matchLength(msg, i, window)
			out['sum'] += np.log2(window+1)/l # to avoid Doeblin condition
		out['subS'].append(msg_)
		out['num'] += 1
	out['h'] = out['sum'] / out['num']
	out['r'] = 1-out['h'] / np.log2(len(msg)) # redundancy, 0<=r<=1
	return out

def matchLength(msg, i, n):
	# Maximum matched length+1, with overlap.
	# i >= n & len(msg) >= i+n
	subS = ''
	for l in range(n):
		msg1 = msg[i:i+l+1]
		for j in range(i-n, i):
			msg0 = msg[j:j+l+1]
			if msg1 == msg0:
				subS = msg1
				break # search for higher l.
	return len(subS) + 1, subS # matched length + 1

def plugIn(msg, w):
	"""! 
	@param msg: a string of characters that will be analyzed for an entropy rate.
	@param w: the length of sub-strings to look for and match on."""
	# compute plug-in (ML) entropy rate
	pmf = pmf1(msg, w)
	out = -sum([pmf[i] * np.log2(pmf[i]) for i in pmf]) / w
	return out, pmf

def pmf1(msg, w):
	# compute the prob mass function for a one-dim discrete rv
	# len(msg)-w occurrences
	lib = {}
	if not isinstance(msg, str):
		msg = ''.join(map(str,msg))
	for i in range(w, len(msg)):
		msg_ = msg[i-w:i]
		if msg_ not in lib:
			lib[msg_] = [i-w]
		else:
			lib[msg_] = lib[msg_] + [i-w]
	pmf = float(len(msg)-w)
	pmf = {i: len(lib[i]) / pmf for i in lib}
	return pmf

if __name__ == "__main__":
	print(plugIn('abcabcabcabcabcabcabcabcabcabcabc', 2))
	print(plugIn('abcabcabcabcabcabcabcabcabcabcabc', 4))
	print(plugIn('abcabcabcabcabcabcabcabcabcabcabc', 8))
	print(plugIn('scrfdgcdythabdtnshdaoercugatnshdstnhdtkx', 4))


	print(konto('abcabcabcabcabcabcabcabcabcabcabc'))
	print(konto('scrfdgcdythabdtnshdaoercugatnshdstnhdtkx'))