"""! @file


@package features
"""


##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))
##\endcond

import pandas as pd
import numpy as np

SYMBOL = 'AAPL'
TYPE = 'volume'

if len(sys.argv) > 1:
	SYMBOL = sys.argv[1].upper()
if len(sys.argv) > 2:
	TYPE = 'dollar'


def get_bsadf(logP, minSL, constant, lags):
	y,x = getYX(logP, constant=constant, lags=lags)
	startPoints, bsadf, allADF = range(0, y.shape[0]+lags-minSL+1), None, []

	for start in startPoints:
		y_, x_ = y[start:], x[start:]
		bMean_, bStd_ = getBetas(y_, x_)
		if bMean_ is None:
			continue # singular matrix or other numpy error, just skip it.
		bMean_ = bMean_[0] #,0]
		bStd_ = bStd_[0,0]**.5
		allADF.append(bMean_ / bStd_)
		if not bsadf or allADF[-1] > bsadf:
			bsadf = allADF[-1]

	out = {'Time': logP.index[-1], 'gsadf': bsadf}
	return out

def getYX(series, constant, lags):
	series_ = series.diff().dropna()
	x = lagDF(series_,lags).dropna()
	x.iloc[:,0] = series.values[-x.shape[0]-1 : -1] # , 0] # lagged level
	y = series_.iloc[-x.shape[0]:].values

	if constant != 'nc':
		x = np.append(x,np.ones((x.shape[0],1)), axis=1)

		if constant[:2] == 'ct':
			trend = np.arange(x.shape[0]).reshape(-1,1)
			x = np.append(x, trend, axis=1)
		if constant == 'ctt':
			x = np.append(x, trend**2, axis=1)
	return y, x

def lagDF(df0, lags):
	df1 = pd.DataFrame()

	if isinstance(lags, int): 
		lags = range(lags+1)
	else:
		logs = [int(lag) for lag in lags]
	for lag in lags:
		df_ = df0.shift(lag).copy(deep=True)
		#df_.columns = [str(i) + '_' + str(lag) for i in df_.columns]
		df1 = df1.join(df_, how='outer', rsuffix = str(lag))
	return df1

def getBetas(y, x):
	try:
		xy = np.dot(x.T, y)
		xx = np.dot(x.T, x)
		xxinv = np.linalg.inv(xx)
		bMean = np.dot(xxinv, xy)
		err = y - np.dot(x, bMean)
		bVar = np.dot(err.T, err) / (x.shape[0] - x.shape[1]) * xxinv
		return bMean, bVar
	except np.linalg.LinAlgError:
		return None, None
	except:
		print(y)
		print(x)
		print(xy)
		print(xx)
		logger.error('getBetas', exc_info=True)

if __name__ == '__main__':

	from data_etl.dollar_bars import GetBars

	bars = GetBars(SYMBOL, sample_factor=1, type=TYPE, resolution="D", LIMIT=500)

	bars['c_log'] = np.log2(bars['c'])

	res = {}
	for i in range(50, len(bars), 1):
		sadf = get_bsadf(bars['c_log'][:i], 50, 'ct', 40)
		res[i] = sadf
		# print(i, sadf)

	x, y = [], []
	for r in res:
		x.append(bars.index.values[res[r]['Time']])
		y.append(res[r]['gsadf'])

	import matplotlib.pyplot as plt


	plt.plot(bars['c'])
	plt.twinx()
	plt.plot(x, y, 'r')
	# plt.plot(bars.iloc[seq]['c'], 'ro')
	# plt.plot(sadf['gsadf'])

	plt.show()



