"""! @file

Fixed-Width Window FracDiff

See Advances in Financial Machine Learning, pp. 83+

This works great! It splits the underlying motion from the noise in a pretty optimal way,
retaining enough "memory" in both without wiping out information content in either.

@package features"""


##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

SYMBOL = 'AAPL'
TYPE = 'volume'
D = None

if len(sys.argv) > 1:
	SYMBOL = sys.argv[1].upper()
if len(sys.argv) > 2 and sys.argv[2].find('d') > -1:
	TYPE = 'dollar'
if len(sys.argv) > 3:
	D = float(sys.argv[3])
##\endcond

def getWeights_FFD(d, thres):
	w, k = [1.], 1

	while True:
		w_ = -w[-1] / k * (d - k + 1)
		if abs(w_) < thres:
			break
		w.append(w_)
		k += 1

	return np.array(w[::-1]).reshape(-1,1)

def fracDiff_FFD(series, d, thres=1e-5): # 1e-5
	# Constant width window (new solution)
	w = getWeights_FFD(d, thres)
	width = len(w) - 1
	df = {}

	for name in series.columns:
		seriesF, df_ = series[name].fillna(method='ffill').dropna(), pd.Series(dtype='float')

		for iloc1 in range(width, seriesF.shape[0]):
			loc0, loc1 = seriesF.index[iloc1 - width], seriesF.index[iloc1]
			if not np.isfinite(series.loc[loc1,name]):
				continue # exclude NAs
			dot = np.dot(w.T, seriesF.loc[loc0:loc1])
			df_.at[loc1] = dot[0] # [0,0]
		df[name] = df_.copy(deep=True)

	df = pd.concat(df, axis = 1)

	return df

def plotMinFFD(bars):
	from statsmodels.tsa.stattools import adfuller

	path, instName = './', 'ES1_Index_Method12'

	out = pd.DataFrame(columns=['adfStat', 'pVal', 'lags', 'nObs', '95% conf', 'corr'])
	df0 = bars

	for d in np.linspace(0, 1, 11):
		df1 = np.log(df0[['c']]) #.resample('1D').last() # downcast to daily obs, if at quicker frequency
		df2 = fracDiff_FFD(df1, d, thres = .01)

		corr = np.corrcoef(df1.loc[df2.index, 'c'], df2['c'])[0,1]

		df2 = adfuller(df2['c'], maxlag=1, regression='c', autolag=None)
		out.at[d] = list(df2[:4]) + [df2[4]['5%']] + [corr] # with critical value

	out.to_csv('./tmp/{}_testMinFFD.csv'.format(instName))

	out[['corr','adfStat']].plot(secondary_y='adfStat')
	plt.axhline(out['95% conf'].mean(), linewidth=1, color='r', linestyle='dotted')
	plt.show()

	return out

if __name__ == "__main__":
	from data_etl.dollar_bars import GetBars

	bars = GetBars(SYMBOL, sample_factor=1/60, bar_type=TYPE, resolution=1)

	if D:
		d = D
	else:
		# run ADF Test to find the minimum D giving us 95% confidence
		adfTest = plotMinFFD(bars)

		d = 0.1
		x = 0
		for idx, row in adfTest.iterrows():
			if row['adfStat'] >= row['95% conf']:
				d = idx
				x = row['adfStat']
			else:
				dist = abs(row['adfStat']-row['95% conf']) / abs(row['adfStat'] - x)
				d = dist * abs(idx-d) + d
				break

		logger.info('running fracDiff_FFD for symbol {} with optimal d={}'.format(SYMBOL, d))

	ffd = fracDiff_FFD(bars[['o', 'h', 'l', 'c']], d, thres=1.0/len(bars)) #thres=1e-5 in the text, but with <500 samples it crashes. 1e-3 is stable in that case.

	plt.plot(bars['c'])
	plt.plot(bars['c']-ffd['c'], 'g')
	plt.twinx()
	plt.plot(ffd['c'], 'r')
	plt.show()