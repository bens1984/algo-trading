"""! @file

Index relationships: create features that estimate how a security has been moving relative to our "index" indicators.

Note: wish we could include .VIX in here, but Finnhub doesn't give it to me yet.

1) Smooth out index data
2) Calculate hourly deltas as % of current price
3) For each security use the analogous EWM of deltas and compare as % of current price

@package features """

##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))
##\endcond

import finnhub
import requests
from datetime import datetime, timedelta
import numpy as np
import pandas as pd
from data_etl.dollar_bars import GetBars
from common.database import DataFrameToDatabase, DeDupeTable, GetTable, ExecuteOnDatabase

SYMBOL = 'XLP'

INDEXES = ['QQQE', 'SPY', 'NDAQ']

RESOLUTION = 60
if len(sys.argv) > 1:
	RESOLUTION = int(sys.argv[1])

def CalculateBeta(symbols, index_bars):
	"""! 
	@param index_bars: bars for each index we want to calculate the beta against
	"""
	for symbol in symbols:
		try:
			bars = GetTable('finnhub_volume_bars_{}'.format(RESOLUTION), WHERE="symbol = '{}'".format(symbol[0]), SUFFIX="ORDER BY t ASC")
			bars = pd.DataFrame(bars)

			if len(bars) < 32:
				logger.debug('fewer than 32 bars for {}. Not enough to calculate Index Betas'.format(symbol[0]))
				continue

			logger.debug('calculating index beta for {} over {} bars'.format(symbol[0], len(bars)))

			ind_sym = index_bars['symbol'].unique()

			out = pd.DataFrame(bars[['finnhub_volume_bars_{}_index'.format(RESOLUTION),'t', 'ts', 'symbol']])
			# out.index = bars['t']
			# del bars['t']

			means = (bars['h']-bars['l'])*0.5 + bars['l']

			# smooth out to weekly and then get changes hour to hour
			deltas = means.rolling(32).mean().diff().fillna(0)

			# convert to delta % of mean price
			deltas = (deltas / means) * 100.0

			for index in ind_sym:
				# get dif from index
				ibars = index_bars.loc[index_bars['symbol'] == index]
				ibars.index = ibars['t']

				d0 = ibars.iloc[ibars.index.searchsorted(bars['ts'])-1]['percent_change']
				d0.index = deltas.index

				d0 = (deltas - d0)

				out['beta_{}'.format(index)] = d0

			# update back into the table

			DataFrameToDatabase(out, 'finnhub_volume_bars_{}'.format(RESOLUTION), INDEX='finnhub_volume_bars_{}_index'.format(RESOLUTION))
		except IndexError:
			logger.error("index_relationships::CalculateBeta", exc_info=True)
			# continue - this one is borked. Just skip it.

def UpdateIndexDeltas():

	logger.debug('loading delta changes for index symbols: {}'.format(str(INDEXES)))

	for ind in INDEXES:
		bars = GetBars(ind, resolution=1, sample_factor=1/RESOLUTION, bar_type='raw')

		# let's use mean of each bar, rather than close (so we're catching more volatility)
		bars['m'] = (bars['h']-bars['l'])*0.5 + bars['l']
		freq = [30, 60, 120, 240]
		for f in freq:
			bars['m_ewm_{}'.format(f)] = bars['m'].rolling(f).mean().fillna(0)
			bars['impulse_{}'.format(f)] = 100.0 * (bars['m']-bars['m_ewm_{}'.format(f)]) / bars['m']

		# smooth out to weekly (these are changing from Sunday @ 6 to Friday @ 6), and then get changes hour to hour
		bars['delta'] = bars['m'].rolling(120).mean().diff().fillna(0)

		# convert to delta % of mean price
		bars['percent_change'] = (bars['delta'] / bars['m']) * 100.0

		del bars['updated_at']; del bars['created_at']
		DataFrameToDatabase(bars, 'index_bars', INDEX=None)

	DeDupeTable('index_bars', keys=['symbol', 't'])

if __name__ == "__main__":
	# we only have 3 indexes right now: QQQE, SPY, NDAQ
	ExecuteOnDatabase('ALTER TABLE "public"."finnhub_volume_bars_{}" ADD COLUMN IF NOT EXISTS "beta_QQQE" float8;'.format(RESOLUTION))

	UpdateIndexDeltas()

	from common.database import GetTable, QueryDatabase

	index_bars = GetTable('index_bars', SUFFIX="ORDER BY symbol, t ASC")
	index_bars = pd.DataFrame(index_bars)

	symbols = QueryDatabase("""SELECT distinct(symbol) FROM finnhub_volume_bars_{} WHERE "beta_QQQE" is NULL""".format(RESOLUTION))

	l = int(np.ceil(len(symbols) / 8))
	split = []
	for i in range(0,8):
		split.append( symbols[i*l:(i+1)*l] )

	from multiprocessing import Pool

	pool = Pool()

	results = []
	for s in split:
		results.append(pool.apply_async(CalculateBeta, [s, index_bars], {})) # args, kwargs

	pool.close()
	pool.join()
	out = [res.get() for res in results]

	# DeDupeTable('index_relationships', keys=['symbol', 't'])