"""! @file

Create some moving average based features we think might have useful information in them.

@package features """

# SYMBOL = 'AAL'

TYPE = 'volume' # one of 'dollar' or 'volume'. Any 2nd argument will switch this to dollar

RESOLUTION = 60

##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))

# if len(sys.argv) > 1:
# 	SYMBOL = sys.argv[1].upper()
if len(sys.argv) > 1:
	RESOLUTION = int(sys.argv[1])
	#TYPE = 'dollar'

import pandas as pd
import numpy as np
# from pandas_datareader import data as web
# import plotly.graph_objects as go
##\endcond


def CalculateAndLoadMABars(symbol):
	# 1. get dollar bars by hour. We'll cache them in the db.
	from data_etl.dollar_bars import GetBars
	from data_etl.imbalance_bars import CalculateOrderDirections
	from data_etl.bid_ask_spread_estimation import corwinSchultz
	from data_etl.candle_maintanence import DoRSI

	try:
		df = GetBars(symbol, sample_factor=1/RESOLUTION, bar_type=TYPE, resolution=1)

		if df is None or not 'c' in df.columns: # garbage in, garbage out
			return

		df['symbol'] = symbol
		df['delta_ts'] = df['ts'].diff()

		# and tick imbalance indicators
		CalculateOrderDirections(df)
		df['tick_imbalance'] = df['sign']
		del df['sign']

		delta = df['c'].diff().fillna(0)
		# 2. calculate moving averages, and convert those to binary feature flags: 1 = above MA, 0 = below MA
		for i in [5, 10, 20, 40, 80, 160, 320]:
			df['c_ma_{}'.format(i)] = df['c'].ewm(span=i, adjust=False).mean()

			df['c_ma_{}_flag'.format(i)] = (df['c'] > df['c_ma_{}'.format(i)])

			# volume/dollars is constant bar to bar, but t is not! Let's do MA of the speed of the bars:
			df['t_ma_{}'.format(i)] = df['delta_ts'].ewm(span=i, adjust=False).mean()

			df['t_ma_{}_flag'.format(i)] = (df['delta_ts'] > df['t_ma_{}'.format(i)]) 

			df['tick_imbalance_ma_{}'.format(i)] = df['tick_imbalance'].ewm(span=i, adjust=False).mean()

			df['delta_ewm_{}'.format(i)] = (delta.ewm(span=i, adjust=False).mean() / df['c']).fillna(0)

		# 2b. add entropy calculation
		ent = CalculateEntropy(np.log2(df['c']))
		if len(ent) == len(df): # very rarely there's an error and it can't calculate the values.
			df['entropy'] = ent

		# 2c. calculate bid/ask spread and volatility
		spread = corwinSchultz(df[['h','l']])
		df['spread'] = spread['Spread']
		df['bid_wanted_in_competition'] = spread['Sigma']
		df['bid_wanted_ma_24'] = spread['Sigma'].rolling(24).mean()

		# 2d. add 24 trading hours high and low as a ratio to current close
		df['high_24_hour_ratio'] = (df['h'].rolling(24).max() - df['c']) / df['c']
		df['low_24_hour_ratio'] = (df['c'] - df['l'].rolling(24).min()) / df['c']

		# 2e. add RSI
		df = DoRSI(df)

		# 3. store
		from common.database import DataFrameToDatabase

		logger.debug('loading {} moving average {} bars for {}'.format(len(df), TYPE, symbol))
		DataFrameToDatabase(df, 'finnhub_{}_bars_{}'.format(TYPE, RESOLUTION), INDEX=None)
	except ValueError:
		logger.error('could not complete MA Bar load for symbol {}'.format(symbol), exc_info=True)
	except Exception:
		logger.error(__name__, exc_info=True)

def CalculateEntropy(series):
	"""! use fracdiff to split the signal, and then calculate entropy on the signal.
	"""
	from features.fracdiff import fracDiff_FFD
	from features.entropy import konto

	out = []

	# this could be estimated first, but [.35,.6] generally works for stocks
	d = 0.35
	ffd = fracDiff_FFD(pd.DataFrame(series), d, thres=1.0/len(series))

	min_ = min(ffd['c'])
	max_ = max(ffd['c'])
	quantile = (max_ - min_) / 10.0
	chars = np.floor((ffd['c'] - min_) / quantile) + 97.0

	data = ''
	res = None
	entropy = []
	for i, val in chars.iteritems():
		data += chr(int(val))
		
		if len(data) >= 10:
			res = konto(data, memory=res)

			entropy.append(res['h'])
			
			data = data[1:]

	# fill in the start with the initial value
	if len(entropy) > 0:
		entropy = [entropy[0]] * (len(series)-len(entropy)) + entropy

	return entropy

if __name__ == "__main__":
	import psycopg2
	# CalculateAndLoadMABars('CARZ')
	# exit()

	from common.database import QueryDatabase, DeDupeTable

	try:
		# to prevent duplicate entries we dedupe by symbol and index (vol/dollar bars have ts as timestamp and t as a serial index. ts isn't unique if one original bar is split into multiple)
		DeDupeTable('finnhub_{}_bars_{}'.format(TYPE, RESOLUTION), keys=['symbol', 't'])

		symbols = QueryDatabase("""SELECT distinct(symbol) FROM "public"."finnhub_stock_candles_1"
					WHERE symbol NOT IN (
						SELECT distinct(symbol) FROM "public"."finnhub_{}_bars_{}" WHERE updated_at > NOW()-INTERVAL '18 HOUR' ) 
					ORDER BY 1 ASC;""".format(TYPE, RESOLUTION)) # updated_at < NOW()-INTERVAL '1 DAY'
	except psycopg2.errors.UndefinedTable:
		symbols = QueryDatabase('SELECT distinct(symbol) FROM "public"."finnhub_stock_candles_1"')


	from multiprocessing import Pool

	pool = Pool()

	results = []
	for sym in symbols:
		results.append(pool.apply_async(CalculateAndLoadMABars, [sym[0]], {})) # args, kwargs

	pool.close()
	pool.join()
	out = [res.get() for res in results]

	# to prevent duplicate entries we dedupe by symbol and index (vol/dollar bars have ts as timestamp and t as a serial index. ts isn't unique if one original bar is split into multiple)
	DeDupeTable('finnhub_{}_bars_{}'.format(TYPE, RESOLUTION), keys=['symbol', 't'])