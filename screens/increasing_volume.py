"""! @file

Increasing Volume

Identify securities that have more and more volume coming in.

@package screens"""


##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
##\endcond


if __name__ == "__main__":
	# we want symbols that have a constant volume for a while, then an increase.
	# 2) of that subset, which ones are moving up/down (some will just hold). Are the buys/sells imbalancing?


	from common.database import QueryDatabase

	query = """
	(SELECT 
		symbol,
		avg(c),
		avg(v)
	FROM finnhub_stock_candles_D
	WHERE to_timestamp(t)::date > NOW() - INTERVAL '180 DAY')
	"""