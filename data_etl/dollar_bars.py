"""! @file

Create non-time-based candles. Dollar Bars and Volume Bars.
See: Advances in Financial Machine Learning

@package data_etl """

SYMBOL = 'AAL'

TYPE = 'volume' # one of 'dollar' or 'volume'. Any 2nd argument will switch this to dollar


##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))
##\endcond

if len(sys.argv) > 1:
	SYMBOL = sys.argv[1]
if len(sys.argv) > 2:
	TYPE = 'dollar'

import pandas as pd
# from pandas_datareader import data as web
# import plotly.graph_objects as go

def GetBars(symbol, sample_factor=1, bar_type="volume", resolution="D", LIMIT=None):
	"""! Helper function to retrieve raw candles, process to volume/dollar bars, and return as DataFrame
	"""

	from common.database import GetTable

	candles = GetTable("finnhub_stock_candles_{}".format(resolution), WHERE="symbol = '{}'".format(symbol), SUFFIX="ORDER BY t")

	if LIMIT:
		candles = candles[-LIMIT:]

	if len(candles) < 10:
		logger.warning("couldn't find enough data for symbol {} with candle resolution {}. Skipping.".format(symbol, resolution))
		return None

	if bar_type == 'raw':
		if sample_factor > 1:
			return pd.DataFrame(ResampleTime(candles, sample_factor))
		else:
			return pd.DataFrame(candles)
	else:
		bars = pd.DataFrame(TimeToVolume(candles, SAMPLE_FACTOR=sample_factor, TYPE=bar_type)) # ToVolume returns 't' as a serial index, we could approximate dates if we wanted using candles start/end dates
		
		return bars

def ResampleTime(candles, SAMPLE_FACTOR):
	"""! Don't resample for volume/dollars, just down sample to longer time spans.
	@param SAMPLE_FACTOR: a number of minutes to resample at.
	"""
	if SAMPLE_FACTOR < 1:
		logger.warning("ResampleTime called with sample_factor of {} < 1. Current function cannot upsample faster than 1 minute.")
		return None

	SAMPLE_FACTOR *= 60 # convert to seconds

	out = []
	sample = candles[0]
	sample['v'] = 0 # it will get added back in right away
	t0 = sample['t']
	for c in candles:

		tN = c['t']

		if tN - t0 > SAMPLE_FACTOR:
			out.append(sample)
			sample = c
			t0 = c['t']
		else:
			sample['h'] = max(sample['h'], c['h'])
			sample['l'] = min(sample['l'], c['l'])
			sample['v'] += c['v']
			sample['c'] = c['c']

	return out

def TimeToVolume(candles, SAMPLE_FACTOR=1.0, TYPE='volume'):
	"""!
	Step through the candles, using another value to re-parse.
	Volume: add up the volumes and each time they cross a threshold make a new bar. Or if one bar is > volume break it in to parts.

	Q: how to automatically configure vol_threshold to something useful? If we're looking at 1 minute bars all day, we get 390 bars,
	which is commonly used by day traders. If they use ticks they get more resolution in the busy times, and less in the slow. Maybe 
	count # of bars and try to reparse to get the same, but with better distribution?

	@param SAMPLE_FACTOR: an over-sample control. At 1 it will try to return the same number of bars as input, > 1 will return more bars,
	and < 1 will return fewer. SAMPLE_FACTOR=2.0 will over-sample by 2x, returning twice as many bars as input.
	"""
	df = pd.DataFrame(candles)

	count = len(df.v)

	if TYPE == 'volume':
		total = sum(df.v)
	else:
		total = sum(df.v * ((df.h-df.l)*0.5 + df.l))

	quant_per_bar = (total / count) / SAMPLE_FACTOR

	logger.debug('reparsing {} bars with {} total {} such that new bars have {} quantity each.'.format(count, total, TYPE, quant_per_bar))

	new_bars = []
	vol_counter = 0
	counter = 0
	# initialize our sampler
	sampled = {'o': None, 'c': df['c'].iloc[0], 'h': 0, 'l': sys.maxsize}
	# loop through all rows, grabbing new samples
	for idx, row in df.iterrows():
		# expand high and low
		sampled['h'] = max(sampled['h'], row['h'])
		sampled['l'] = min(sampled['l'], row['l'])
		# update close, and use close time
		sampled['c'] = row['c']
		# if we don't have an open, use this one
		if not sampled['o']:
			sampled['o'] = row['o']
			sampled['ts'] = row['t']

		if TYPE == 'volume':
			vol = row['v']
		else:
			vol = row['v'] * ((row['h']-row['l']) * 0.5 + row['l'])

		# if vol is enough for several bars we loop through and split it up:
		while vol > 0:
			delta = min(vol, quant_per_bar-vol_counter)

			vol_counter += delta
			vol -= delta

			if (quant_per_bar - vol_counter) == 0:
				sampled['v'] = vol_counter
				sampled['t'] = counter

				new_bars.append(sampled.copy())

				# reset sampler to this bar
				sampled['o'] = row['o']
				sampled['h'] = row['h']
				sampled['l'] = row['l']
				sampled['c'] = row['c']
				sampled['ts'] = row['t']
				counter += 1
				vol_counter = 0

	return new_bars

if __name__ == '__main__':
	"""! 
	Get a symbol.
	Break the time based data down into new dollar or volume based bars.
	"""

	from common.database import GetTable

	candles = GetTable("finnhub_stock_candles_D", WHERE="symbol = '{}'".format(SYMBOL), SUFFIX="ORDER BY t")

	logger.debug('processing {} candles for symbol {}'.format(len(candles), SYMBOL))

	vol_candles = TimeToVolume(candles, SAMPLE_FACTOR=1, TYPE=TYPE)

	df = pd.DataFrame(vol_candles)
	# df['ts'] = pd.to_datetime(df['t'], unit='s')
	# if we plot it with actual time it is effectively the same as time-based. When we work with it we want to treat it as an equally sampled signal.

	trace1 = {
		'x': df.t,
		'open': df.o,
		'close': df.c,
		'high': df.h,
		'low': df.l,
		'type': 'candlestick',
		'name': '{} candles'.format(TYPE),
		'showlegend': True
	}

	data = [trace1]
	# Config graph layout
	layout = go.Layout({
		'title': {
			'text': "{} - {}".format(SYMBOL, TYPE),
			'font': {
				'size': 15
			}
		}
	})

	fig = go.Figure(data=data, layout=layout)
	fig.write_html("Stock Prices.html")
	fig.show()