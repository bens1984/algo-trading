"""! @file

# Imbalance Bars

Calculate imbalance bars, give the sequence of signs (1,-1) over a series of observations.

@package data_etl"""

SYMBOL = 'AAPL'
TYPE = 'volume'

##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))

import pandas as pd
import numpy as np
##\endcond

if len(sys.argv) > 1:
	SYMBOL = sys.argv[1].upper()
if len(sys.argv) > 2:
	TYPE = 'dollar'

def CalculateOrderDirections(bars):
	"""! 
	See Advances in Financial Machine Learning, pp. 29-32

	When close is higher record a 1, when it's lower record a -1

	@param bars: DataFrame with 'c' (close) values for the series. Assume ordered in time ascending order already.
	"""
	p_t = bars.iloc[0]['c']
	signs = []
	sign = 0
	if 'imbalance_tick' in bars: # if already initialized, start from row 0
		sign = bars.iloc[0]['imbalance_tick']

	for idx, row in bars.iterrows():
		if row['c'] > p_t:
			sign = 1
		elif row['c'] < p_t:
			sign = -1
		# else: sign remains the same

		signs.append(sign)

		p_t = row['c']

	bars['sign'] = signs

def Imbalance(df):

	# theta-T: cumulative sum of the order directions
	cs = [0]
	for idx, row in df.iterrows():
		cs.append(row['sign'] + cs[-1])

	df['imbalance'] = cs[0:-1] # drop last value
	print(df['imbalance'])

def ProbabilityOfDirection(df):
	# 2P{b_t = 1} + 1 ??
	df['prob_direction'] = df['sign'].ewm(span=5, adjust=False).mean()
	df['imbalance_smooth'] = df['imbalance'].ewm(span=5, adjust=False).mean()

	# mv_avg = [0]
	# for s in signs:
	# 	mv_avg.append(mv_avg[-1] * 0.9 + s * 0.1)

	# return mv_avg

def SampleSignal(df):
	# compute a signal that says "sample!" vs "skip"

	signal = [0]
	for i, row in df.iterrows():
		if abs(row['imbalance']) >= row['imbalance_smooth'] * abs(row['prob_direction']):
			signal.append(row['c'])
		else:
			signal.append(signal[-1])
	
	return signal[1:]


if __name__ == '__main__':

	from common.database import GetTable

	candles = GetTable("finnhub_stock_candles_D", WHERE="symbol = '{}'".format(SYMBOL), SUFFIX="ORDER BY t")

	from data_etl.dollar_bars import TimeToVolume

	df = pd.DataFrame(TimeToVolume(candles, SAMPLE_FACTOR=1, TYPE=TYPE))
	CalculateOrderDirections(df)

	Imbalance(df)
	ProbabilityOfDirection(df)

	import matplotlib.pyplot as plt

	plt.plot(df['sign'])
	plt.show()

	plt.plot(list(abs(df['imbalance']))) #[-2000:])

	# plt.plot(list(df['prob_direction']))
	# plt.show()

	# plt.plot(signs[-2000:])
	# plt.plot(mv_avg[-2000:])
	# plt.show()

	# expectation = []
	# x = 0
	# for c, m in zip(cum_sum, mv_avg):
	# 	x = x * 0.9 + c * 0.1
	# 	expectation.append(x * m)

	# plt.plot(expectation[-2000:])
	# plt.show()
	plt.plot(df['imbalance_smooth'] * abs(df['prob_direction']))
	plt.show()

	TIB = SampleSignal(df)
	plt.plot(TIB[-2000:])
	plt.plot(list(df.c)[-2000:])
	plt.show()
