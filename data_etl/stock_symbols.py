"""! @file

Get All Stock symbols from Finnhub.

Then it gets the company profiles for all Common Stocks.

@package data_etl
"""

# set true to use sandbox environment
SANDBOX = False

##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))
##\endcond

import finnhub

def getCompanyProfile(sym):
	from time import sleep
	WAIT = 1 # exponential back off for rate limiting

	done = False

	while not done:
		try:
			profile = finnhub_client.company_profile2(symbol=sym)
			WAIT = 1
			done = True
		except finnhub.exceptions.FinnhubAPIException as e:
			if e.status_code == 429: # rate limit, exponential back off
				sleep(WAIT)
				WAIT *= 2

	return profile

def DoCompanyProfiles():
	"""! get any missing profiles for Common Stocks and start filling them in. """

	from common.database import QueryDatabase
	stocks = QueryDatabase("""SELECT symbol FROM finnhub_stock_symbols WHERE "country" IS NULL AND "type" = 'Common Stock' OR "type" = 'ETP' """)

	# now get the company profile for each symbol:
	table2 = []
	for st in stocks:
		entry = {'symbol': st[0]}
		sym = entry['symbol']

		profile = getCompanyProfile(sym)

		for key in profile:
			entry[key] = profile[key]

		table2.append(entry)

		if len(table2) > 49 or sym == stocks[len(stocks)-1][0]:
			df = pd.DataFrame(table2)

			logger.debug(df.head())

			from common.database import DataFrameToDatabase

			DataFrameToDatabase(df, 'finnhub_stock_symbols', INDEX='symbol')

			table2 = []

if __name__ == '__main__':
	try:
		import pandas as pd
		import finnhub

		# Setup client
		if SANDBOX:
			finnhub_client = finnhub.Client(api_key=os.environ.get('FINN_HUB_SANDBOX'))
		else:
			finnhub_client = finnhub.Client(api_key=os.environ.get('FINN_HUB_KEY'))

		stocks = finnhub_client.stock_symbols('US')

		logger.info('got {} stock symbols'.format(len(stocks)))

		df = pd.DataFrame(stocks)

		from common.database import DataFrameToDatabase

		DataFrameToDatabase(df, 'finnhub_stock_symbols', INDEX='symbol')

		DoCompanyProfiles()

	except:
		logger.error(__name__, exc_info=True)