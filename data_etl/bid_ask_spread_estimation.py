"""! @file

Bid-Ask spread estimation

chp. 19 in Advances in Financial Machine Learning

@package data_etl """


##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))
##\endcond

import pandas as pd
import numpy as np

SYMBOL = 'AAPL'
TYPE = 'volume'

if len(sys.argv) > 1:
	SYMBOL = sys.argv[1].upper()
if len(sys.argv) > 2:
	TYPE = 'dollar'

# -----------------------
def getBeta(series, sl):
	hl = series[['h','l']].values
	hl = np.log(hl[:,0]/hl[:,1]) ** 2
	hl = pd.Series(hl, index = series.index)
	beta = hl.rolling(2).sum() # pd.rolling_sum(hl, window = 2)
	beta = beta.rolling(sl).mean() #(beta, window=sl)
	return beta.dropna()

def getGamma(series):
	h2 = series['h'].rolling(2).max() # pd.rolling_max(series['h'], window=2)
	l2 = series['l'].rolling(2).min() # pd.rolling_min(series['l'], window=2)
	gamma = np.log(h2.values / l2.values) ** 2
	gamma = pd.Series(gamma, index=h2.index)
	return gamma.dropna()

def getAlpha(beta, gamma):
	den = 3-2*2**.5
	alpha = (2**.5-1) * (beta**.5) / den
	alpha -= (gamma/den) ** .5
	alpha[alpha<0] = 0 # set negative alphas to 0 (see p. 727 of Corwin and Schultz [2012])
	return alpha.dropna()

def corwinSchultz(series, sl=1):
	# Note: S < 0 iif alpha < 0
	beta = getBeta(series, sl)
	gamma = getGamma(series)
	alpha = getAlpha(beta, gamma)
	spread = 2 * (np.exp(alpha)-1) / (1+np.exp(alpha))
	startTime = pd.Series(series.index[0:spread.shape[0]], index=spread.index)
	spread = pd.concat([spread, startTime], axis=1)
	spread.columns = ['Spread', "Start_Time"] # 1st loc used to compute beta
	spread['Sigma'] = getSigma(beta, gamma)
	return spread

def getSigma(beta, gamma):
	"""! derive Becker-Parkinson volatility from Corwin-Schultz estimated levels
	"""
	k2 = (8 / np.pi) ** .5
	den = 3-2*2**.5
	sigma = (2**.5-1) * beta ** .5 / (k2 * den)
	sigma += (gamma / (k2 ** 2 * den)) ** .5
	sigma[sigma < 0] = 0
	return sigma
# -----------------------

if __name__ == "__main__":
	from data_etl.dollar_bars import GetBars

	bars = GetBars(SYMBOL, resolution=1, sample_factor=1/60, bar_type=TYPE)

	df = bars[['h','l']]

	spread = corwinSchultz(df)
	spread.index = spread['Start_Time']

	import matplotlib.pyplot as plt
	plt.plot(spread['Spread'])
	plt.twinx()
	plt.plot(spread['Sigma'], 'g')
	# plt.plot(bars[['h','l']])
	plt.show()