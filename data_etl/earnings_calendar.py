"""! @file

Earnings calendars

@package data_etl"""


##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))

import finnhub
import pandas as pd
import numpy as np
##/endcond

SANDBOX = False

if __name__ == "__main__":

	# Setup client
	if SANDBOX:
		finnhub_client = finnhub.Client(api_key=os.environ.get('FINN_HUB_SANDBOX'))
	else:
		finnhub_client = finnhub.Client(api_key=os.environ.get('FINN_HUB_KEY'))


	res = finnhub_client.recommendation_trends('INTC')
	for r in res:
		print(r)

	#res = finnhub_client.earnings_calendar(_from="2021-09-28", to="2021-10-28", symbol="", international=False)

	#print(res)