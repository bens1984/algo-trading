"""! @file

Get social sentiment.

Todo: this is the same code calling a different finnhub function as recommendation_trends.py. Refactor to share classes!

@package data_etl"""


##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))

import finnhub
import pandas as pd
import numpy as np
##/endcond

SANDBOX = False

def doRecommendation(sym):
	from time import sleep
	WAIT = 1 # exponential back off for rate limiting
	ATTEMPTS = 10
	done = False

	while not done and ATTEMPTS > 0:
		ATTEMPTS -= 1

		try:
			results = finnhub_client.stock_social_sentiment(sym)
			WAIT = 1
			done = True
		except finnhub.exceptions.FinnhubAPIException as e:
			if e.status_code == 429: # rate limit, exponential back off
				sleep(WAIT)
				WAIT *= 2
			elif e.status_code == 403: # access denied. Probably a security that requires a premium subscription
				logger.warning("Error 403: access to this resource denied: {}".format(sym), exc_info=True)
				results = None
				done = True
			else:
				raise e
		except requests.exceptions.ReadTimeout as e:
			sleep(WAIT)
			WAIT *= 2

	return results

if __name__ == "__main__":

	# Setup client
	if SANDBOX:
		finnhub_client = finnhub.Client(api_key=os.environ.get('FINN_HUB_SANDBOX'))
	else:
		finnhub_client = finnhub.Client(api_key=os.environ.get('FINN_HUB_KEY'))

	# get all our symbols, and away we go!
	from common.database import QueryDatabase, DataFrameToDatabase
	symbols = QueryDatabase("SELECT symbol FROM finnhub_stock_symbols WHERE type='Common Stock'")

	# see if we have any done already. Only get symbols with current month reported, or NULL (aka no data when we last checked)
	try:
		res = QueryDatabase("""SELECT distinct(symbol) FROM finnhub_social_sentiment 
						WHERE updated_at > NOW() - INTERVAL '5 DAY');""")
		exclude = [r[0] for r in res]
		logger.debug('have {} symbols already done, with {} in total.'.format(len(exclude),len(symbols)))
		symbols = [s for s in symbols if not s[0] in exclude]
		logger.debug('have {} in total after exclusion.'.format(len(symbols)))
	except:
		pass # ignore. Table may not exist

	df = pd.DataFrame()
	counter = 0
	for sym in symbols:
		res = doRecommendation(sym[0])

		if res != {}:
			# for r in res: # convert everything to strings. Entries can be lists and this causes pd to freak.
			# 	res[r] = str(res[r])
			print(res)
			df = df.append(pd.DataFrame([res]))
		else:
			# insert empty row to indicate no data available.
			df = df.append(pd.DataFrame([{'symbol': sym[0]}]))

		counter += 1
		if counter % 60 == 0: # on free teir we get 60 requests/min, so do our DB update while we wait
			DataFrameToDatabase(df, 'finnhub_social_sentiment', INDEX=None)
			df = pd.DataFrame()

	if len(df) > 0:
		DataFrameToDatabase(df, 'finnhub_social_sentiment', INDEX=None)

	# to prevent duplicate entries we dedupe by symbol and timestamp
	# DeDupeTable('finnhub_social_sentiment', keys=['symbol', 'period'])