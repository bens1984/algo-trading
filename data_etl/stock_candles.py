"""! @file

Get historical Stock Candles!

First Argument options:
* D - get all available symbols for the last 365 days at day resolution
* 1 - get selected high volume securities, as far back as available at 1 minute resolution
* E - get selected ETPs as far back as available at 1 minute
* S - get selected index funds as far back as available at 1 minute

@package data_etl"""

##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))
##\endcond

import finnhub
import requests
from datetime import datetime, timedelta
import numpy as np


SANDBOX = False

# if "D" will only get most recent year.
RESOLUTION = "D"

if len(sys.argv) > 1:
	RESOLUTION = sys.argv[1]

def getCandles(finnhub_client, sym, resolution, from_ts, to_ts):
	from time import sleep
	WAIT = 1 # exponential back off for rate limiting
	ATTEMPTS = 10
	done = False

	while not done and ATTEMPTS > 0:
		ATTEMPTS -= 1

		try:
			results = finnhub_client.stock_candles(sym, resolution, from_ts, to_ts)
			WAIT = 1
			done = True
		except finnhub.exceptions.FinnhubAPIException as e:
			if e.status_code == 429: # rate limit, exponential back off
				sleep(WAIT)
				WAIT *= 2
			elif e.status_code == 403: # access denied. Probably a security that requires a premium subscription
				logger.warning("Error 403: access to this resource denied: {}".format(sym), exc_info=True)
				results = None
				done = True
			else:
				raise e
		except requests.exceptions.ReadTimeout as e:
			sleep(WAIT)
			WAIT *= 2

	return results

def getCandlesForSymbol(finnhub_client, symbol, EARLIEST=None, RESOLUTION=1):
	from datetime import datetime, timedelta
	import pandas as pd
	from common.database import DataFrameToDatabase, QueryDatabase

	table_name = 'finnhub_stock_candles_{}'.format(RESOLUTION)
	# Supported resolution includes 1, 5, 15, 30, 60, D, W, M . Some timeframes might not be available depending on the exchange.
	# if requesting more than 1 month at a time it returns the most recent month, it seems.
	if RESOLUTION != 'D' and RESOLUTION != 'W' and RESOLUTION != 'M': # for times we'll do 30 days at a time, for D/W/M we can go a full year
		INCREMENT = 30
		RESOLUTION = int(RESOLUTION) # in case it came through as a string
	else:
		INCREMENT = 365

	res = None
	# res = QueryDatabase("""SELECT sync_to_t, sync_complete 
	# 						FROM "finnhub_stock_candles_sync_log" 
	# 						WHERE symbol = '{}' AND sync_table = '{}'""".format(symbol, table_name))
	
	if res is None or res == []:
		latest, complete = None, datetime.min
	else:
		latest, complete = res[0]

	if latest is None:
		latest = 0 # we don't have this symbol, need to request back to the start

	# did we try this one recently? If yes, skip it
	if complete >= (datetime.today() - timedelta(hours=48)):
		return False

	end = datetime.now()

	max_ts = 0
	done = False
	while not done:
		start = max(latest, (end-timedelta(days=INCREMENT)).timestamp())
		res = getCandles(finnhub_client, symbol, RESOLUTION, int(start), int(end.timestamp()))

		# when we get past the beginning of what finnhub will give us s is 'ok' for a few, but all others are None. Then eventually 's' = 'no_data'
		# the request returns 200 None when it has data and a 200 15 when it's past the earliest available.
		if res and len(res) > 0 and res['s'] == 'ok' and res['o'] != None:

			try:
				max_ts = max(max_ts, max(res['t']))

				df = pd.DataFrame(res)

				df['symbol'] = symbol

				DataFrameToDatabase(df, table_name, INDEX=None)

				end = end-timedelta(days=INCREMENT)

				if end.timestamp() < latest or (EARLIEST and end < EARLIEST):
					done = True
			except ValueError:
				logger.error(__name__, exc_info=True)
				return True

		else:
			done = True

	LogSyncRecord(symbol, datetime.now(), max_ts, table_name)
	return True

def LogSyncRecord(symbol, sync_complete, sync_to_t, table_name):
	"""! record this sync in our log table """
	sync_complete = sync_complete.strftime('%Y-%m-%d %H:%M:%S.%f')

	from common.database import ExecuteOnDatabase
	ExecuteOnDatabase("""INSERT INTO "finnhub_stock_candles_sync_log" (symbol, sync_to_t, sync_complete, sync_table ) VALUES (
							'{}',
							{},
							'{}',
							'{}')
						ON CONFLICT ("symbol", "sync_table") DO UPDATE 
						SET sync_to_t = {}, sync_complete = '{}', sync_table = '{}'""".format(symbol, sync_to_t, sync_complete, table_name, sync_to_t, sync_complete, table_name))

def SetupSyncTable():
	"""! log most recent sync for each symbol so we can optimally skip up-to-date symbols.
	"""
	from common.database import ExecuteOnDatabase
	ExecuteOnDatabase("""CREATE TABLE IF NOT EXISTS "finnhub_stock_candles_sync_log" ( 
							id serial PRIMARY KEY, 
							created_at TIMESTAMP DEFAULT NOW(), 
							updated_at TIMESTAMP DEFAULT NOW(),  
							symbol text NOT NULL,
							sync_to_t int8 NOT NULL,
							sync_complete TIMESTAMP NOT NULL,
							sync_table text NOT NULL,
							UNIQUE(symbol, sync_table)
							)""")

def GetHighResolutionScreeners():
	screen = """SELECT
	*
FROM (
	SELECT
		*,
		(std / price) "variance",
		("52high" - "52low") "range",
		(std_vol / volume) "vol_variance",
		(max_volume / volume) "vol_spike"
	FROM (
		SELECT
			symbol,
			count(v) "ct",
			AVG(c) "price",
			STDDEV_SAMP(c) "std",
			AVG(v) "volume",
			STDDEV_SAMP(v) "std_vol",
			max(h) "52high",
			min(l) "52low",
			max(v) "max_volume"
		FROM
			"finnhub_stock_candles_D"
		WHERE
			t > {}
		GROUP BY
			symbol
		ORDER BY
			symbol ASC) t1
	WHERE
		NOT price IS NULL
		AND
		-- must have traded 200 out of 252 last trading days. Gives us ~9500
		ct > 200
		-- average close price greater than $10, to filter out penny stocks. ~5700. $20 takes out another 700.
		-- average less than $400 takes out 200 more.
		AND price > 10
		AND price < 400
		-- more than 100000 traded per day?? I'm not confident in this one. Strips out 2600, leaving 3150.
		AND volume > 50000
		-- price standard deviation over the year is more than $1? Takes out 200.
		AND std > 1) t2
WHERE
	-- variance over the year. Greater than 5%? Takes it to 2100. Meh. Greater than 10%? Takes out another 1/2...
	"variance" > 0.05
	-- volume variance over the year, looking for stocks that changed. > 1 gets it to 392! > 0.75 = 850 with price > 20 (1146 with price > 10)
	AND "vol_variance" > 0.75
	-- we want spikey volume, get biggest day > 15x the average day. Results in ~400 symbols.
	-- AND "vol_spike" > 15
	;""".format(int((datetime.today()-timedelta(days=365)).timestamp()))

	from common.database import QueryDatabase

	# res = QueryDatabase(screen)
	res = QueryDatabase("SELECT * FROM screened_spiky_volume_stocks")

	symbols = [r[0] for r in res]

	logger.debug('screened for {} symbols to get high rez OHLC data'.format(len(symbols)))

	return symbols


if __name__ == '__main__':
	SetupSyncTable()

	logger.debug('syncing candles at resolution {}'.format(RESOLUTION))

	# Setup client
	if SANDBOX:
		finnhub_client = finnhub.Client(api_key=os.environ.get('FINN_HUB_SANDBOX'))
	else:
		finnhub_client = finnhub.Client(api_key=os.environ.get('FINN_HUB_KEY'))

	from common.database import QueryDatabase, DeDupeTable

	if True: # hack to trigger deep sync of D symbols that were already screened
		stocks = QueryDatabase("""SELECT distinct(symbol) FROM "high_volume_ETPs" UNION SELECT distinct(symbol) FROM "screened_spiky_volume_stocks" """)
	else:
		if RESOLUTION != 'E':
			if RESOLUTION == 'S':
				# get our index-approximate securities (Finnhub won't give us ES1! or futures indexes)
				stocks = [('QQQE',),('SPY',),('NDAQ',)]
			else:
				stocks = QueryDatabase("SELECT symbol, ipo FROM finnhub_stock_symbols WHERE type='Common Stock' OR type='ETP' ORDER BY symbol") # LIMIT 50 OFFSET 50")
		else:
			# do high rez ETP pull. Use our materialized view that pulls ETPs with average daily volume > 500,000
			stocks = QueryDatabase("""SELECT symbol FROM "public"."high_volume_ETPs" ORDER BY symbol""")

	res = QueryDatabase("SELECT symbol, sync_complete FROM finnhub_stock_candles_sync_log WHERE sync_table = '{}'".format('finnhub_stock_candles_{}'.format(RESOLUTION)))

	logger.debug("syncing {} symbols, with {} syncs already logged".format(len(stocks), len(res)))

	syncs = {}
	for r in res:
		syncs[r[0]] = r[1]

	cutoff = datetime.today() - timedelta(hours=12)

	most_recent_ts = {}
	screened = GetHighResolutionScreeners() # None
	if RESOLUTION == 'D':
		EARLIEST = datetime.min # datetime.today()-timedelta(days=365)
	else:
		EARLIEST = datetime.min
		if RESOLUTION == 'E' or RESOLUTION == 'S':
			RESOLUTION = 1
		else:
			screened = GetHighResolutionScreeners()

		# get most recent sample in the db
		res = QueryDatabase("SELECT symbol, max(t) FROM finnhub_stock_candles_1 GROUP BY symbol")
		for r in res:
			most_recent_ts[r[0]] = datetime.fromtimestamp(r[1])

	newdata = False
	for st, i in zip(stocks, range(0,len(stocks))):
		early = EARLIEST
		if st[0] in most_recent_ts:
			early = most_recent_ts[st[0]]

		if (st[0] in syncs and syncs[st[0]] > cutoff) or early > cutoff:
			continue

		# check if we have a screened list, and if it's in the list:
		if not screened is None and not st[0] in screened:
			continue

		if i % np.ceil(len(stocks) * 0.01) == 0:
			print("progress: {}%".format(i/len(stocks)))

		x = getCandlesForSymbol(finnhub_client, st[0], EARLIEST=early, RESOLUTION=RESOLUTION)
		newdata = newdata or x

	if newdata:
		logger.debug('deduplicating table "finnhub_stock_candles_{}", on "symbol" and "t"'.format(RESOLUTION))
		# to prevent duplicate entries we dedupe by symbol and timestamp
		DeDupeTable('finnhub_stock_candles_{}'.format(RESOLUTION), keys=['symbol', 't'])
