"""! @file

Add calculated analysis to candle bar tables.

In particular I'm excited about imbalance ticks, and volume weighting of moving averages.

TODO: make this a lot more efficient. Use the previous tick and volume tick and go from there, for each symbol.

@package data_etl """


##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))

import pandas as pd
import numpy as np
from common.database import QueryDatabase, ExecuteOnDatabase, DataFrameToDatabase
##/endcond

# def PushTicksToDB(data, table_name):
# 	"""! have to build our own, because it's a multi-index table
# 	"""
# 	from common.database import ExecuteOnDatabase

# 	command = ""
# 	for idx, row in data.iterrows():
# 		command += """UPDATE "{}" SET imbalance_tick={} WHERE symbol='{}' AND t={};""".format(table_name, row['sign'], row['symbol'], row['t'])

# 	ExecuteOnDatabase(command)

def DoRSI(df, span0=14):
	"""! Calculate RSI and stochastic RSI for a dataframe of OHLC data 
	@param df: DataFrame of close ('c') data
	@param span0: look back window size. Default = 14
	"""
	if not 'c' in df or len(df) < span0:
		return df

	df['diff'] = (df['c'].diff().fillna(0) / df['c'])
	# take all positive deltas and smooth 
	df['avg_gain'] = df[df['diff']>0]['diff'].rolling(span0).mean()
	# interpolate to fill in the negative deltas
	df['avg_gain'] = df['avg_gain'].interpolate()

	df['avg_loss'] = df[df['diff']<0]['diff'].rolling(span0).mean()
	df['avg_loss'] = df['avg_loss'].interpolate()

	df['RSI'] = 1.0 - (1.0 / (1.0 + abs(df['avg_gain']/df['avg_loss']).fillna(0)))

	x = df['RSI'].rolling(span0)
	df['stoch_RSI'] = (df['RSI'] - x.min()) / (x.max()-x.min())

	del df['diff']; del df['avg_gain']; del df['avg_loss']

	return df

def DoOneSymbolImbalanceTicks(table_name, sym):
	from data_etl.imbalance_bars import CalculateOrderDirections

	# fastest solution would be to assume each close is locked to the next open. But can we?? There could be gaps, so for each symbol we'll re-calc the whole sequence
	data = QueryDatabase("""SELECT "{}_index", "c", "t", "v", "imbalance_tick", "imbalance", "imbalance_volume_weighted" 
		FROM "public"."{}" 
		WHERE symbol = '{}' 
		ORDER BY t;""".format(table_name, table_name, sym))

	if len(data) < 2:
		return None

	table = []
	for d in data:
		table.append({'{}_index'.format(table_name): d[0], 'symbol': sym, 'c': d[1], 't': d[2], 'v': d[3], 'imbalance_tick': d[4], 'imbalance': d[5], 'imbalance_volume_weighted': d[6]})

	df = pd.DataFrame(table)

	# find max t that has imbalance already calculated
	t = df[~df['imbalance'].isnull()]['t'].max()
	if not t is None:
		# drop all rows up to t
		df = df[df['t'] >= t]

	logger.debug('calculating {} ticks for {}'.format(len(df), sym))

	CalculateOrderDirections(df)

	df['imbalance_tick'] = df['sign']

	df.loc[1:, ('imbalance')] = np.cumsum(df.loc[1:, ('sign')]) + df.iloc[0]['imbalance'] # don't add the first row in again
	# df1['imbalance'] = df1['cumsum'].ewm(span=10, adjust=False).mean() # don't push smoothing in here, we can perform that step during analysis.
	df.loc[1:, ('imbalance_volume_weighted')] = np.cumsum(df.loc[1:, ('imbalance_tick')] * df.loc[1:,('v')]) + df.iloc[0]['imbalance_volume_weighted']

	del df['sign']; del df['c']; del df['v']; del df['t'] # no need to push these back in, they aren't being changed.

	# Add RSI and Stochastic RSI:
	df = DoRSI(df)

	DataFrameToDatabase(df, table_name, INDEX = '{}_index'.format(table_name))

	return True

def DoImbalanceTicks():

	# get all candle tables...
	from common.database import ExecuteOnDatabase, DataFrameToDatabase
	from data_etl.imbalance_bars import CalculateOrderDirections

	tables = QueryDatabase("SELECT table_name FROM information_schema.tables WHERE table_name LIKE 'finnhub_stock_candles_%' ORDER BY 1;")

	logger.debug('got {} tables like "finnhub_stock_candles_%" to update.'.format(len(tables)))

	for t in tables:
		# this will also have the sync log table, so skip it.
		if t[0].find('sync') > -1:
			continue

		logger.debug('adding imbalance, imbalance_tick column to table {}'.format(t[0]))

		# make sure the table has the column we want
		ExecuteOnDatabase('ALTER TABLE "public"."{}" ADD COLUMN IF NOT EXISTS "imbalance_tick" int4;'.format(t[0]))
		ExecuteOnDatabase('ALTER TABLE "public"."{}" ADD COLUMN IF NOT EXISTS "imbalance" float8;'.format(t[0]))
		ExecuteOnDatabase('ALTER TABLE "public"."{}" ADD COLUMN IF NOT EXISTS "imbalance_volume_weighted" float8;'.format(t[0]))

		done = False
		while not done:
			# fastest solution would be to assume each close is locked to the next open. But can we?? There could be gaps, so for each symbol we'll re-calc the whole sequence
			data = QueryDatabase("""SELECT "{}_index", symbol, c, t, v FROM "public"."{}" WHERE symbol IN (
				SELECT distinct(symbol) FROM "public"."{}" WHERE imbalance_tick IS NULL LIMIT 100
				) ORDER BY symbol, t """.format(t[0], t[0], t[0]))

			logger.debug("got {} data points to update".format(len(data)))

			if len(data) > 0:
				table = []
				for da in data:
					entry = {}
					for d, s in zip(da, ['{}_index'.format(t[0]), 'symbol','c','t', 'v']):
						entry[s] = d
					table.append(entry)
				df = pd.DataFrame(table)

				symbols = set(df['symbol']) # set returns unique collection of strings

				logger.debug('got {} symbols for table {} to update imbalance ticks'.format(len(symbols), t[0]))

				df2 = pd.DataFrame()
				for sym in symbols:
					df1 = df.loc[df['symbol'] == sym]

					CalculateOrderDirections(df1)

					df1['imbalance'] = np.cumsum(df1['sign'])
					# df1['imbalance'] = df1['cumsum'].ewm(span=10, adjust=False).mean() # don't push smoothing in here, we can perform that step during in analysis.
					df1['imbalance_volume_weighted'] = np.cumsum(df1['sign'] * df1['v'])

					df2 = df2.append(df1)

				logger.debug('pushing {} ticks to {}'.format(len(df2), t[0]))

				df2['imbalance_tick'] = df2['sign']
				del df2['sign']
				DataFrameToDatabase(df2, t[0], INDEX = '{}_index'.format(t[0]))
				# PushTicksToDB(df2, t[0])
				# and don't worry about dedupe since we're just filling in a new column
			else:
				done = True

def DoImbalanceTicksParallel():
	from common.database import ExecuteOnDatabase, DataFrameToDatabase
	
	tables = QueryDatabase("SELECT table_name FROM information_schema.tables WHERE table_name LIKE 'finnhub_stock_candles_%' ORDER BY 1;")

	logger.debug('got {} tables like "finnhub_stock_candles_%" to update.'.format(len(tables)))

	for t in tables:
		# this will also have the sync log table, so skip it.
		if t[0].find('sync') > -1:
			continue

		# make sure the table has the column we want
		ExecuteOnDatabase('ALTER TABLE "public"."{}" ADD COLUMN IF NOT EXISTS "imbalance_tick" int4;'.format(t[0]))
		ExecuteOnDatabase('ALTER TABLE "public"."{}" ADD COLUMN IF NOT EXISTS "imbalance" float8;'.format(t[0]))
		ExecuteOnDatabase('ALTER TABLE "public"."{}" ADD COLUMN IF NOT EXISTS "imbalance_volume_weighted" float8;'.format(t[0]))
		ExecuteOnDatabase('ALTER TABLE "public"."{}" ADD COLUMN IF NOT EXISTS "RSI" float8;'.format(t[0]))
		ExecuteOnDatabase('ALTER TABLE "public"."{}" ADD COLUMN IF NOT EXISTS "stoch_RSI" float8;'.format(t[0]))

		symbols = QueryDatabase("""SELECT distinct(symbol) FROM "public"."{}" WHERE imbalance_tick IS NULL;""".format(t[0]))
		logger.debug('got {} symbols to update in table {}'.format(len(symbols), t[0]))

		from multiprocessing import Pool

		pool = Pool()

		threads = []
		for sym in symbols:
			threads.append(pool.apply_async(DoOneSymbolImbalanceTicks, [t[0], sym[0]], {})) # args, kwargs

		pool.close()
		pool.join()
		# this actually executes the threaded functions. Nothing gets returned so results will be empty.
		results = [res.get() for res in threads]

		# df = pd.DataFrame()
		# for r in results:
		# 	df = df.append(r)

		# df['imbalance_tick'] = df['sign']
		# del df['sign']; del df['c']; del df['v']; del df['t'] # no need to push these back in, they aren't being changed.
		# DataFrameToDatabase(df, t[0], INDEX = '{}_index'.format(t[0]))

if __name__ == "__main__":
	# DoOneSymbolImbalanceTicks('finnhub_stock_candles_1', 'MDY')
	# exit()

	DoImbalanceTicksParallel()