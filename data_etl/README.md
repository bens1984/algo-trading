# Data ETL

Pipeline code to update our cached copies of financial data that is used to build and test our models.

## Process

* Make sure our symbol list is up to date: run stock_symbols.py
* Acquire day data - run stock_candles.py D
* From there screen for some to get high-rez data for. There is a materialized view in the cloud DB, and a "screener" query in stock_candles
* Run stock_candles.py 1 to get all the high-rez data
* run candle_maintanence.py - this will fill in some 1st order analysis data (tick imbalances)
* Look to features/moving_average_trends.py to produce reflected views of calculated trends
* recommendation_trends, social_sentiment, earnings_calendar - these are fun, maybe? Try running them, maybe we can use it.