"""! @file

@package unit-tests """

##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))
##\endcond

from datetime import datetime

def CreateDeleteTable():
	"""! try creating and deleting a table
	"""
	try:
		from common.database import ExecuteOnDatabase

		cmd = """Create Table IF NOT EXISTS unit_test_table_54 
			(
				id serial PRIMARY KEY,
				created_at TIMESTAMP DEFAULT NOW(),
				updated_at TIMESTAMP DEFAULT NOW(),
				column1 VARCHAR (50) UNIQUE NOT NULL,
				column2 float
			);"""

		logger.debug(cmd)
		ExecuteOnDatabase(cmd)

		cmd = "DROP TABLE unit_test_table_54;"

		logger.debug(cmd)
		ExecuteOnDatabase(cmd)
	except:
		logger.error('CreateDeleteTable', exc_info=True)

def DataframeTest():
	try:
		from common.database import ExecuteOnDatabase
		ExecuteOnDatabase("DROP TABLE IF EXISTS unit_test_dataframe_table")

		import pandas as pd

		table = []
		for i in range(0,100):
			entry = {'user_id': i, 'name': 'me, myself and i', 'record': {'x':10-i, 'y':i*2.34}, 'list': [1,2,3,4,i], 'ts': datetime.now() }
			table.append(entry)

		from common.database import DataFrameToDatabase
		DataFrameToDatabase(pd.DataFrame(table), 'unit_test_dataframe_table')

		ExecuteOnDatabase("DROP TABLE IF EXISTS unit_test_dataframe_table")
	except:
		logger.error('DataframeTest', exc_info=True)

if __name__ == '__main__':

	CreateDeleteTable()

	DataframeTest()