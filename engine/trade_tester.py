"""! @file

# Trade Tester

Given a proposed trade (or many): evaluate.
1. What would the outcome be?
	- use 3 barrier method to check for profit-taking, stop-loss, or time out
2. Risk?

Uses stock_1 table and stock_60 OHLC only, doesn't need any enrichment or features, but needs to symbols reflected back in to the 60 table.

@package engine"""



##\cond
import os, sys
p = os.path.dirname(os.path.abspath('./algo-trading'))
sys.path.append(p)

import common.log
logger = common.log.SetLogFile('../logs/{}.log'.format(__name__))

import pandas as pd
import numpy as np
# import matplotlib.pyplot as plt
from datetime import datetime, timedelta

# SYMBOL = 'AAPL'
TYPE = 'volume'
D = None

# if len(sys.argv) > 1:
# 	SYMBOL = sys.argv[1].upper()
if len(sys.argv) > 2 and sys.argv[2].find('d') > -1:
	TYPE = 'dollar'
if len(sys.argv) > 3:
	D = float(sys.argv[3])
##\endcond

def ApplyStopLossProfitTaking(close, events, profitTaking, stopLoss, molecule=None):
	"""! Apply stop loss/profit taking, if it takes place before t1 (end of event)

	@param close: pandas series of price points with datetime index.
	@param events: DataFrame with 2 columns: 
		* open -> the datetime to open the position
		* days -> number of days after the open to close the event, if it hasn't hit a horizontal barrier
		* trgt -> unit width of the horizontal barriers (space from open to upper and lower)
	@param profitTaking: a factor that multiplies trgt to set the width of the upper barrier (or 0 for none)
	@param stopLoss: a factor that multiplies trgt to the the width of the lower barrier (or 0 for none)
	@param molecule: list of subset event indecies to be considered (for multi-processing use)
	"""
	if not 'trgt' in events:
		return None

	if molecule:
		events_ = events.loc[molecule]
	else:
		events_ = events
	out = events_.copy(deep = True)

	if profitTaking != 0:
		pt = profitTaking * events_['trgt']
	else:
		pt = pd.Series(index = events.index) # NaNs
	if stopLoss != 0:
		sl = stopLoss * events_['trgt']
	else:
		sl = pd.Series(index = events.index) # NaNs

	for loc, row in events.iterrows(): # events['t1'].fillna(close.index[-1]).iteritems():
		# x = close.index.get_loc(t1)
		# x = close.iloc[(np.searchsorted(close.index, t1) - 1).clip(0)]
		if row['days']:
			try:
				x = close.index.searchsorted(pd.to_datetime(row['open'] + timedelta(days=row['days'])))
				x = close.index[x-1] # get the adjusted date that is actually in the close sequence
				df0 = close.loc[pd.to_datetime(row['open']) : x] # path prices. from open to open+t1
			except (KeyError, TypeError, AttributeError) as e:
				logger.error(str(row['open']))
				logger.error(str(x))
				logger.error(min(events['open']))
				logger.error(max(events['open']))
				logger.error(close)
				logger.error(close.index, exc_info = True)
				raise e
			if len(df0) == 0:
				return None

			out.loc[loc, 'end'] = df0[-1] # price at vertical barrier
		else:
			x = datetime.max
			df0 = close.loc[pd.to_datetime(row['open']) : ]
			if len(df0) == 0:
				return None

			out.loc[loc, 'end'] = None

		# df0 = close.loc[pd.to_datetime(close.index[loc]) : x] # path prices. Why is this starting at close.index[loc] ? That would be the first or 2nd sample?? Why step through the beginning?
		out.loc[loc, 'o'] = df0.iloc[0] # store opening price

		# df0 = (df0 / close[loc]-1) * events_.at[loc, 'side'] # path returns

		s0 = df0[df0 < sl[loc]+out.loc[loc,'o']].index.min() # earliest stop loss
		p0 = df0[df0 > pt[loc]+out.loc[loc,'o']].index.min() # earliest profit taking

		out.loc[loc, 'sl'] = s0
		out.loc[loc, 'pt'] = p0

		try:
			if not pd.isnull(s0): # update position close position to the first of stop/profit
				# if the first value in s0 is NaT it won't check the 2nd value. This catches p0 null or not
				out.loc[loc, 'end'] = df0.loc[min(s0,p0)]
			elif not pd.isnull(p0):
				# this catches s0 = NaT but p0 = datetime
				out.loc[loc, 'end'] = df0.loc[p0]
		except ValueError:
			logger.error("ApplyStopLossProfitTaking::ValueError", exc_info=True)
			return None # something is wrong, let's just ignore the trash.

	return out

def getDailyVolatility(close, span0=100):
	"""!
	@param close: pandas series of price points, with a datetime index. See pd.DatetimeIndex or pd.to_datetime.
	@param span0: span of exponentially weighted moving average, in days. 

	@return: a series of intraday volatility estimates, based on an EWM of the standard deviation of daily sampled price points."""
	df0 = close.index.searchsorted(close.index.round("D") - pd.Timedelta(days=1)) # round to day, then subtract a day
	df0 = np.unique(df0[df0>0]) # only get one index for each day. Believe it is the first sample (close of first minute)

	df0 = pd.Series(close.iloc[df0-1].index, index=close.iloc[df0].index) # each day and the previous day
	# df0 = pd.Series(close.index[df0-1], index=close.index[close.shape[0] - df0.shape[0]:])

	daily_returns = close.loc[df0.index] / close.loc[df0.values].values - 1 # daily returns -- divide yesterday by today.
	#df0 = close.loc[df0.index] / close.loc[df0.values].values - 1

	std = daily_returns.ewm(span=span0).std()

	return df0, std

def getDailyVolatilityHighLow(data, span0=100):
	"""! Use high and lows rolling max/min to estimate daily price variance.
	@param data: pandas dataframe with 'h' and 'l' series, and datetime index
	@param span0: window size in data param's index increments. If data is in minutes span0 will be in minutes.
	"""
	df0 = data.index.searchsorted(data.index.round("D")) # - pd.Timedelta(days=1)) # round to day, then subtract a day
	df0 = np.unique(df0[df0>0]) # only get one index for each day. Believe it is the first sample (close of first minute)

	#df0 = pd.Series(data.iloc[df0-1].index, index=data.iloc[df0].index) # each day and the previous day
	df0 = data.iloc[df0-1].index

	df1 = pd.DataFrame()
	df1['h_max'] = data['h'].rolling(span0).max()
	df1['l_min'] = data['l'].rolling(span0).min()
	df1['spread'] = df1['h_max'] - df1['l_min']

	daily_returns = df1.loc[df0]['spread']

	# apply rolling again. Maybe this 2nd time is too much? But the spread is pretty spikey..
	return df0, daily_returns.rolling(span0).mean()

def NaiveTrader(symbols):
	"""! Attempt 1. This tests every trade starting at 9:30 every day. """
	for sym in symbols:

		bars = GetBars(sym[0], sample_factor=1, bar_type='raw', resolution=1) #, LIMIT=500)
		bars['ts'] = pd.to_datetime(bars['t'], unit='s')
		bars.index = bars['ts']

		days, std = getDailyVolatility(bars['c'], span0=20)

		vol = pow(std.mean(), 0.5) # use the square-root of the mean of the daily standard deviation of prices.
		logger.debug('volatility estimated, {}: {}'.format(sym[0], vol))

		# test trades against raw bars. But would make intelligent guesses on entry points from dollar/volume
		df = bars # GetBars(sym[0], sample_factor=1/60, bar_type='volume', resolution=1)
		if not 'ts' in df.columns:
			continue # GIGO

		df.index = pd.to_datetime(df['ts'], unit='s')

		events = []
		for d in days:
			events.append({'open': datetime(d.year,d.month,d.day,15,30), 'days': 7, 'trgt': 5})
		events = pd.DataFrame(events)
		# events = pd.DataFrame([{'open': datetime(2021,11,12,15,30), 't1': 7, 'trgt': 100.0}, \
		# 	{'open': datetime(2021,11,19,14,30), 't1': 7, 'trgt': 100.0}, \
		# 	{'open': datetime(2021,11,16,15,5), 't1': 7, 'trgt': 100.0}])
		res = ApplyStopLossProfitTaking(bars['c'], events, vol, -vol)

		if not res is None:
			res['symbol'] = sym[0]
			res['variance'] = vol

			DataFrameToDatabase(res, 'trade_testing_results', INDEX=None)

	DeDupeTable('trade_testing_results', keys=['symbol', 'open'])

def CumSumSymbol(sym):
	from data_etl.dollar_bars import GetBars
	from common.database import QueryDatabase, DataFrameToDatabase, DeDupeTable, GetTable

	# downsample to every hour, volume bars.
	# we now store these, so go right to that table:
	#bars = GetBars(sym[0], sample_factor=1/60, bar_type='volume', resolution=1)
	bars = GetTable("finnhub_volume_bars_60", WHERE="symbol='{}'".format(sym[0]), SUFFIX="ORDER BY t ASC")
	bars = pd.DataFrame(bars)

	if bars is None or not 'h' in bars or len(bars) < 1:
		return

	logger.debug('simulating trades for {} over {} hours of bars'.format(sym[0], len(bars)))

	# use log of prices to make changes over years more comperable
	bars['h_log'] = np.log2(bars['h'])
	bars['l_log'] = np.log2(bars['l'])
	bars['c_log'] = np.log2(bars['c'])

	# can't do this, because it's not unique. Some bars share the same minute in ts. We'll make it unique later when we need it.
	#bars.set_index(pd.to_datetime(bars["ts"], unit='s'), inplace=True)

	# some 'ts' will be shared, not unique, across neighboring bars. Make a copy, pick the first unique entry, and strip out the rest
	df1 = bars.copy(deep=True)
	df1 = df1.iloc[df1['ts'].searchsorted(df1['ts'].unique())]
	df1.index = pd.to_datetime(df1['ts'], unit='s')

	# days, std = getDailyVolatility(df1['c'], span0=20)
	# average high/low spread over 16 hour periods.
	days, daily_ATR = getDailyVolatilityHighLow(df1[['h','l']], span0=16)
	daily_ATR = daily_ATR.dropna() # ** 0.5
	
	# vol = pow(std.mean(), 0.5) # use the square-root of the mean of the daily standard deviation of prices.
	# logger.debug('volatility estimated, {}: {}'.format(sym[0], vol))

	# ---------------- Get Event openings
	from features.cumsum import getTEvents

	# determine how far a step is enough to trigger a new event:
	#bar_range = max(bars['h_log']) - min(bars['l_log']) / 10
	bar_range = np.mean(np.log2(daily_ATR)) # ** 2.0))

	# use log of close, look for changes that are 1/10th of total range
	idx, flags = getTEvents(bars['c_log'], bar_range)

	# since volume is constant, delta-time will vary. Use up/down movement in delta-time to determine places other players are entering/exiting the market!
	bar_range = bars['ts'].diff().fillna(0)
	bar_range += min(bar_range.iloc[1:]) # add DC offset so all x >= 0

	# 1/2 of delta-time range typically results in a quantity of points > than the c_log points, above.
	h = (max(bar_range) - min(bar_range.iloc[1:])) * 0.5

	idx2, flags2 = getTEvents(bar_range.fillna(0), h)

	# get timestamp of each entry from idx
	times = pd.to_datetime(bars.loc[idx + idx2]['ts'], unit='s')
	# -----------------

	# calculate trades on raw bars, not volume weighted ones.
	bars = GetBars(sym[0], sample_factor=1, bar_type='raw', resolution=1)
	bars['ts'] = pd.to_datetime(bars['t'], unit='s')
	bars.index = bars['ts']

	events = []
	x = None
	for d, f, i in zip(times, flags+flags2, range(0,len(times))):
		# adding 'reason': a flag 0/1 if the event was chose because the negative cumsum or the positive was hit
		event = {'open': d, 'days': 7, 'trgt': 5, 'reason': f}

		try:
			# set 'trgt' to volatility at the open
			x = min(max(daily_ATR.index.searchsorted(d) - 1, 0), len(daily_ATR) - 1)
			if x == -1:
				continue # if the event is in the very early range the EWM returns 0s

			event['trgt'] = daily_ATR.iloc[x] * 0.5

			if i > len(flags):
				event['reason'] += 2 # offset the 'time' reasons to 2,3 and leave the 'price' reasons at 0,1
			events.append(event)
		except (IndexError, KeyError):
			logger.error(d)
			logger.error(x)
			logger.error(len(daily_ATR))
			logger.error('CumSumSymbol', exc_info=True)

	events = pd.DataFrame(events)

	res = ApplyStopLossProfitTaking(bars['c'], events, 1.5, -1)
	# res = ApplyStopLossProfitTaking(bars['c'], events, vol*1.5, -vol) # * 0.5)

	if not res is None:
		res['symbol'] = sym[0]
		res['variance'] = 1.0

		DataFrameToDatabase(res, 'trade_testing_results_v2', INDEX=None)

def DoManySymbols(symbols):
	for sym in symbols:
		CumSumSymbol(sym)

def CumulativeSumTrader(symbols):
	"""! Attempt 2. This uses cumulative sums for entry points. """
	# split symbols into 8 sub-lists:
	l = int(np.ceil(len(symbols) / 8))
	split = []
	for i in range(0,8):
		split.append( symbols[i*l:(i+1)*l] )

	from common.database import DeDupeTable

	from multiprocessing import Pool

	pool = Pool()

	results = []
	for spl in split:
		results.append(pool.apply_async(DoManySymbols, [spl], {})) # args, kwargs

	pool.close()
	pool.join()
	out = [res.get() for res in results]

	DeDupeTable('trade_testing_results_v2', keys=['symbol', 'open'])

if __name__ == "__main__":
	from data_etl.dollar_bars import GetBars
	from common.database import QueryDatabase, DataFrameToDatabase, DeDupeTable
	import psycopg2

	try:
		DeDupeTable('trade_testing_results_v2', keys=['symbol', 'open'])
		symbols = QueryDatabase("""SELECT distinct(symbol) 
					FROM finnhub_stock_candles_1
					WHERE symbol NOT IN (SELECT distinct(symbol) FROM trade_testing_results_v2 WHERE updated_at > NOW() - INTERVAL '18 HOUR');""")
	except psycopg2.errors.UndefinedTable:
		symbols = QueryDatabase("""SELECT distinct(symbol) FROM finnhub_stock_candles_1""")

	# CumSumSymbol(('FLOW',))
	# exit()

	# bars = GetBars('ASHR', resolution=1, sample_factor=1/60, bar_type='raw')
	# bars = bars.iloc[bars['t'].searchsorted(bars['t'].unique())]
	# bars.index = pd.to_datetime(bars['t'], unit='s')
	# print(bars.head())

	# print(getDailyVolatilityHighLow(bars[['h','l']], span0=20))
	# exit()

	CumulativeSumTrader(symbols)
	