#!/bin/bash

# pull in new data, update calculated tables, generate tested trade data
#  and be ready to run some algorithmic trading!


# run from one directory up!
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# relocate to src directory to execute scripts:
cd $DIR
cd ..


python3 data_etl/stock_candles.py 1

python3 data_etl/candle_maintanence.py

python3 features/moving_average_trends.py

python3 features/index_relationships.py

python3 engine/trade_tester.py